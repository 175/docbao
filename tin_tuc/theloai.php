

<?php
$idTL = $_GET["idTL"];
settype($idTL, "int");
?>

<?php
$tin = getTenTheLoai($idTL);
$row_tin = mysqli_fetch_assoc($tin);
?>
<div class="category_page">
    <h5 class="forum_name">
        <span><?php echo $row_tin["TenTheLoai"] ?></span></h5>

    <div class="category_news_top">
        <ul>
            <?php
            $tin = TinTheoTheLoaiTin_HaiTin($idTL);

            while ($row_tin = mysqli_fetch_assoc($tin)) {
                ?>
                <li>
                    <a class="photo"
                       href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                        <img width="340" height="255"
                             src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>"
                             alt="<?php echo $row_tin["TieuDe"] ?>"></a>
                    <h4>
                        <a href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                            <?php echo $row_tin["TieuDe"] ?>
                        </a></h4>
                    <p><?php echo $row_tin["TomTat"] ?></p>
                </li>

                <?php
            }
            ?>
        </ul>
        <div class="clrb">
        </div>
    </div>

    <!-- qc admicro  -->
    <div >

        <a href="http://www.chungcuhn365.net/2017/06/chung-cu-valencia-long-bien.html">
            <img src="images/canho.png">
        </a>
<!--        --><?php
//        $quangcao = quangcao(8);
//        $row_quangcao = mysqli_fetch_assoc($quangcao);
//        ?>
<!--        <a target="black" href="--><?php //echo $row_quangcao['UrlQuangCao'] ?><!--">-->
<!--            <img src="images/--><?php //echo $row_quangcao['UrlImages'] ?><!--"-->
<!---->
<!--                 href="--><?php //echo $row_quangcao['UrlQuangCao'] ?><!--"/>-->
<!--        </a>-->
    </div>

    <!-- end qc admicro -->

    <div class="category_news_list">
        <ul>
            <?php
            $sotin1trang = 5;

            if (isset($_GET["trang"])) {
                $trang = $_GET["trang"];
                settype($trang, "int");
            } else {
                $trang = 1;
            }
            $from = ($trang - 1) * $sotin1trang + 2;
            $tin = TinTheoTheLoai_PhanTrang($idTL, $from, $sotin1trang);
            while ($row_tin = mysqli_fetch_assoc($tin)) {
                ?>
                <li>
                    <a class="photo"
                       href="index.php?p=chitiettin&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                        <img width="206"
                             src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>"
                             alt="<?php echo $row_tin['TieuDe'] ?>"></a>
                    <div class="text">
                        <h5>
                            <a href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                                <?php echo $row_tin['TieuDe'] ?>
                            </a></h5>
                        <span class="time"><?php echo $row_tin['TimeDangBai'] ?></span>
                        <p><?php echo $row_tin['TomTat'] ?></p>
                    </div>
                    <div class="clrb">
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>

    <?php
    $i = 1;
    $i++;
    $t = getTinTrongTheLoai($idTL);
    $tongsotin = mysqli_num_rows($t);
    $tongsotrang = ceil($tongsotin/$sotin1trang);
    if($i >= $tongsotrang) {
        ?>
        <div style="padding: 0 0" class="button_bot">
            <span><a class="btn_next"
                     href="index.php?p=tintrongtheloai&idTL=<?php echo $idTL ?>&trang=<?php echo $i ?>">
                    Xem thêm »</a></span>
        </div>
        <?php
    }
    ?>

</div>
<div class="detail_page">
    <h5 class="forum_name">Tin đọc nhiều</h5>
    <div class="related_news">
        <ul>
            <?php
            $tin = getTinDocNhieu($idTL);

            while ($row_tin = mysqli_fetch_assoc($tin)) {
                ?>
                <li style="margin-bottom: 5px;">
                    <a class="photo"
                       href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                        <img width="165" height="124"
                             src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>"
                             alt="<?php echo $row_tin["TieuDe"] ?>"></a>
                    <h6 style="min-height: 64px;">
                        <a href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                            <?php echo $row_tin["TieuDe"] ?>
                            <span style="font: 11px;"><i>(<?php echo $row_tin["TimeDangBai"] ?>)</i></span></a></h6>
                </li>
                <?php
            }
            ?>
            <br clear="all"></ul>
        <div class="clrb">
        </div>
    </div>
</div>