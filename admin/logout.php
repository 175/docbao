<?php
    session_start();
    header('Content-Type: text/html; charset=UTF-8');

    if (session_destroy()) {
        header('Location: login.php');
    } else {
        echo 'Đăng xuất không thành công';
    }

    echo '<br><a href="index.php">Bấm vào đây để quay lại trang chủ<br></a>';

?>