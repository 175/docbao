<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chỉnh sửa tin tức</title>
    <link type="text/css" rel="stylesheet" href="css/css.css">
    <link type="text/css" rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <?php
        require ("connection.php");
        if (isset($_POST["save"])) {
            $IdTinTuc = $_POST["IdTinTuc"];
            $TieuDe = $_POST["TieuDe"];
            $TomTat = $_POST["TomTat"];
            $NoiDung = $_POST["NoiDung"];

            $nameImages = $_FILES["UrlImages"]["name"];
            $size = $_FILES["UrlImages"]["size"];
            $tmp_name = $_FILES["UrlImages"]["tmp_name"];
            $folder = "../images/tintuc/";

            $IdLoaiTin = $_POST["IdLoaiTin"];
            $IdUser = $_POST["IdUser"];
            $IdBienTap = $_POST["IdBienTap"];
            $idTheLoai = $_POST["idTheLoai"];
            $TinNoiBat = $_POST["TinNoiBat"];
            $TimeDangBai =  date("y-m-d h:m:s" );
            $NguonTin = $_POST["NguonTin"];

            $Images = $folder . $nameImages;

            if (!($size <= 3 * 1024 * 1024)) {
                echo 'file phải <= 3MB. Xin vui lòng chọn Ảnh khác';
            } else {
                $kt = move_uploaded_file($tmp_name, $Images);
                if ($kt) {
                    if ($TieuDe == "" || $TomTat == "" || $NoiDung == "" ) {
                        echo 'Xin mời nhập tất cả các trường theo yêu cầu';
                    } else {
                        $sql = "update tintuc set 
                        TieuDe = '$TieuDe',
                        TomTat = '$TomTat',
                        NoiDung = '$NoiDung',
                        UrlImages = '$nameImages',
                        IdLoaiTin= '$IdLoaiTin',
                        IdUser= '$IdUser',
                        IdBienTap= '$IdBienTap',
                        idTheLoai= '$idTheLoai',
                        TinNoiBat= '$TinNoiBat',
                        TimeDangBai    = '$TimeDangBai',
                        NguonTin = '$NguonTin'
                        WHERE IdTinTuc = '$IdTinTuc'
                        ";

                        $result = mysqli_query($connect, $sql);

                        if ($result) {
                            sleep(1);
                            header('Location: index.php?p=tintuc');
                        } else {
                            echo 'Lỗi';
                        }
                    }
                } else {
                    echo ' chỉnh sửa thất bại';
                }
            }


        }

        $IdTinTuc = "";
        $TieuDe= "";
        $TomTat= "";
        $NoiDung= "";
        $UrlImages= "";
        $ImagesHienTin= "";
        $IdLoaiTin= "";
        $IdUser= "";
        $IdBienTap= "";
        $idTheLoai= "";
        $TinNoiBat= "";
        $TimeDangBai= "";

        if (isset($_GET["id"])) {
            $IdTinTuc = $_GET["id"];
//            exit($IdTinTuc);
            $sql = "select * from tintuc WHERE IdTinTuc = $IdTinTuc";
            $result = mysqli_query($connect, $sql);
            if($result) {
                while ($data = mysqli_fetch_array($result)) {
                    $TieuDe = $data["TieuDe"];
                    $TomTat = $data["TomTat"];
                    $NoiDung = $data["NoiDung"];
                    $UrlImages = $data["UrlImages"];
                    $ImagesHienTin = $data["ImagesHienTin"];
                    $IdLoaiTin = $data["IdLoaiTin"];
                    $IdUser = $data["IdUser"];
                    $IdBienTap = $data["IdBienTap"];
                    $idTheLoai = $data["idTheLoai"];
                    $TinNoiBat = $data["TinNoiBat"];

                }
            } else {
                echo 'Thao tác hệ thống lỗi';
            }
        }

        ?>

        <h3>Chỉnh sửa tin tức</h3>
        <form method="post" name="form_tintic" enctype="multipart/form-data">
            <table class="table" style="color: black">
                <caption style="color: white">Danh sách chỉnh sửa tin tức</caption>
                <input type="hidden" name="IdTinTuc" value="<?php echo "$IdTinTuc" ; ?>">
                <tr>
                    <td>Tiêu đề</td>
                    <td><input type="text" name="TieuDe" value="<?php echo "$TieuDe"; ?> "></td>
                </tr>

                <tr>
                    <td>Tóm tắt</td>
                    <td><input type="text" name="TomTat" value="<?php echo "$TomTat"; ?> "></td>
                </tr>

                <tr>
                    <td>Nội dung</td>
                    <td>
<!--                        <input type="text" name="NoiDung" value="--><?php //echo "$NoiDung"; ?><!-- ">-->
                        <textarea name="NoiDung" id="editor" cols="30" rows="10">
                            <?php echo "$NoiDung"; ?>
                        </textarea>
                        <script>
                            CKEDITOR.replace('editor');
                        </script>
                    </td>
                </tr>
                <tr>
                    <td>Link Ảnh</td>
                    <td><input type="file" name="UrlImages" value="<?php echo "$UrlImages"; ?> "></td>
                </tr>
                <tr>
                    <td>Loại tin</td>
                    <td>
                        <select name="IdLoaiTin" >
                            <?php
                            $sql = "select * from loaitin";
                            $result = mysqli_query($connect, $sql);
                            while ($data = mysqli_fetch_array($result)) {
                                ?>
                                <option value="<?php echo $data["IdLoaiTin"];?>"><?php echo $data["TenLT"] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Nhân viên: </td>
                    <td>
                        <select name="IdUser" id="">
                            <?php
                            $sql = "select * from user";
                            $result = mysqli_query($connect, $sql);
                            while ($data = mysqli_fetch_array($result)) {
                                ?>
                                <option value="<?php echo $data["IdUser"];?>"><?php echo $data["UserName"]; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Biên tập</td>
                    <td>
                        <select name="IdBienTap" id="">
                            <?php
                            $sql = "select * from banbientap";
                            $result = mysqli_query($connect, $sql);
                            while ($data = mysqli_fetch_array($result)) {
                                ?>
                                <option value="<?php echo $data["IdBienTap"];?> "><?php echo $data["HoTen"];?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Thể loại</td>
                    <td>
                        <select name="idTheLoai" id="">
                            <?php
                            $sql = "select * from theloai";
                            $result = mysqli_query($connect, $sql);
                            while ($data = mysqli_fetch_array($result)) {
                                ?>
                                <option value="<?php echo $data["idTheLoai"];?> "><?php echo $data["TenTheLoai"];?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Tin nóng</td>
                    <td>
                        <select name="TinNoiBat" id="">
                            <option value="1">Có</option>
                            <option value="0">Không </option>
                        </select>
                    </td>

                </tr>

                <tr>
                    <td>Tin Tiêu Điểm trong tuần</td>
                    <td>
                        <select name="TinTieuDiem" id="">
                            <option value="1">Có</option>
                            <option value="0">Không </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Nguồn tin</td>
                    <td>
                        <input type="text" name="NguonTin">
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="save" value="Lưu thông tin"></td>
                </tr>
            </table>
        </form>

    </div>
</div>


</body>
</html>