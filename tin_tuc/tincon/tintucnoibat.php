

<div class="news_top">
    <div class="news_top_hd">
        <span class="time"></span>
        <div class="news_top_tabs">
            <a id="news-top-tabs-1" href="javascript:void(0);" class="on">Tin nổi bật</a> <a
                    style="background-color: #01BBF2; color: #ffffff" id="news-top-tabs-2"
                    href="javascript:void(0);">Sự kiện nổi bật;
                <img src="images/icon_hot_1.gif" height="10px"></a>
        </div>
    </div>


    <div class="news_top_ct" id="news-top-ct-1" style="display: block;">

        <div class="ct_left">
            <?php
            $tintuc_sukien = getTinNoiBat_DauTrang();
            $i = 0;
            while ($tintuc_sukien_show = mysqli_fetch_assoc($tintuc_sukien)) {
                ;
                ?>
                <div id="itm-news<?php echo $i++ ?>" class="news_item" style="display: none;">
                    <a class="photo"
                       href="index.php?p=chitiettin&idTL=<?php echo $tintuc_sukien_show['idTheLoai'] ?>&idTinTuc=<?php echo $tintuc_sukien_show['IdTinTuc'] ?>">
                        <div class="crop">
                            <img src="images\tintuc\<?php echo $tintuc_sukien_show["UrlImages"] ?>"
                                 alt="<?php echo $tintuc_sukien_show["TieuDe"] ?>" width="400">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="index.php?p=chitiettin&idTL=<?php echo $tintuc_sukien_show['idTheLoai'] ?>&idTinTuc=<?php echo $tintuc_sukien_show['IdTinTuc'] ?>">
                                <?php echo $tintuc_sukien_show["TieuDe"] ?></a></h4>
                        <p><?php echo $tintuc_sukien_show["TomTat"] ?></p>
                    </div>
                </div>

                <?php
            }
            ?>
        </div>


        <div class="ct_right">
            <ul class="news_lst">
                <?php
                $tintuc_sukien = getTinNoiBat_DauTrang();
                $i = 0;
                while ($tintuc_sukien_show = mysqli_fetch_assoc($tintuc_sukien)) {
                    ?>
                    <li><a onmouseover="Slidenews(<?php echo $i++; ?>)"
                           href="index.php?p=chitiettin&idTL=<?php echo $tintuc_sukien_show['idTheLoai']; ?>&idTinTuc=<?php echo $tintuc_sukien_show['IdTinTuc'] ?>">

                            <?php echo $tintuc_sukien_show["TieuDe"] ?>
                        </a></li>

                    <?php
                }
                ?>
                <div style="font-weight: bold; line-height: 35px; display:none">Tin doanh nghiệp</div>
                <li style="display:none"><a
                            href="index.php?p=chitiettin&idTL=<?php echo $tintuc_sukien_show['idTheLoai']; ?>&idTinTuc=<?php echo $tintuc_sukien_show['IdTinTuc'] ?>">
                        <?php echo $tintuc_sukien_show["TieuDe"] ?></a></li>
            </ul>

        </div>
        <div class="clrb">
        </div>

    </div>

    <script language="javascript" type="text/javascript">
        var curPos1 = 0;
        var timer1 = setTimeout("Slidenews(0)", 9000);

        function Slidenews(index1) {
            clearTimeout(timer1);

            document.getElementById("itm-news" + curPos1).style.display = "none";
            document.getElementById("itm-news" + index1).style.display = "";

            curPos1 = index1;
            index1 = (index1 == 9) ? 0 : (index1 + 1);
            timer1 = setTimeout("Slidenews(" + index1 + ")", 9000);
        }
    </script>


    <div style="display: none;" class="news_top_ct" id="news-top-ct-2">
        <div class="ct_left">
            <?php
            $tintuc_sukien = getSuKienNoiBat();
            $i = 0;
            while ($tintuc_sukien_show = mysqli_fetch_assoc($tintuc_sukien)) {
            ?>
            <div id="itm-news-hot<?php echo $i;
            echo $i++; ?>" class="news_item" style="">
                <a class="photo"
                   href="index.php?p=chitiettin&idTL=<?php echo $tintuc_sao_show['idTheLoai'] ?>&idTinTuc=<?php echo $tintuc_sukien_show['IdTinTuc'] ?>">
                    <div class="crop">
                        <img src="images\tintuc\<?php echo $tintuc_sukien_show["UrlImages"] ?>"
                             alt="<?php echo $tintuc_sukien_show["TieuDe"] ?>" width="400">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="index.php?p=chitiettin&idTL=<?php echo $tintuc_sao_show['idTheLoai'] ?>&idTinTuc=<?php echo $tintuc_sukien_show['IdTinTuc'] ?>">
                            <?php echo $tintuc_sukien_show["TieuDe"] ?>
                        </a></h4>
                    <p><?php echo $tintuc_sukien_show["TomTat"] ?></p>
                </div>
            </div>
        </div>
        <div class="ct_right">
            <ul class="news_lst">
                <?php
                $tintuc_sukien = getSuKienNoiBat();
                $i = 0;
                while ($tintuc_sukien_show = mysqli_fetch_assoc($tintuc_sukien)) {

                    ?>
                    <li><a onmouseover="Slidenewshot(<?php $i++ ?>)"
                           href="index.php?p=tintrongtheloai&p_tl_lt=LoaiTin&idTL=<?php echo $tintuc_sukien_show['idTheLoai']?>&idLoaiTin=<?php echo $tintuc_sukien_show['IdLoaiTin'] ?>"><strong>
                                <?php echo $tintuc_sukien_show["TenLT"] ?>
                            </strong></a><br>
                        <a href="index.php?p=chitiettin&idTL=<?php echo $tintuc_sukien_show['idTheLoai'] ?>idLoaiTin=<?php echo $tintuc_sukien_show['IdLoaiTin'] ?>&idTinTuc=<?php echo $tintuc_sukien_show['IdTinTuc'] ?>"
                           onmouseover="Slidenewshot(<?php $i++ ?>)" style="font-weight: normal;">
                            <?php echo $tintuc_sukien_show["TieuDe"] ?></a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div class="clrb">
        </div>
    </div>

    <?php
    }
    ?>
    <script language="javascript" type="text/javascript">
        var curPos2 = 0;
        var timer2 = setTimeout("Slidenewshot(1)", 9000);

        function Slidenewshot(index2) {
            clearTimeout(timer2);

            document.getElementById("itm-news-hot" + curPos2).style.display = "none";
            document.getElementById("itm-news-hot" + index2).style.display = "";

            curPos2 = index2;
            index2 = (index2 == 5) ? 0 : (index2 + 1);
            timer2 = setTimeout("Slidenewshot(" + index2 + ")", 9000);
        }
    </script>

</div>


<script type="text/javascript">
    $("#news-top-tabs-1").click(function () {
        $(this).addClass("on");
        $("#news-top-tabs-2").removeClass("on");
        $("#news-top-ct-2").hide();
        $("#news-top-ct-1").show();
    });

    $("#news-top-tabs-2").click(function () {
        $(this).addClass("on");
        $("#news-top-tabs-1").removeClass("on");
        $("#news-top-ct-1").hide();
        $("#news-top-ct-2").show();
    });
</script>


<br clear="all">



