<?php require_once ("connection.php"); ?>

<div>
    <h3> Quản lý Sự Kiện: </h3>
    <table class="table" style="color: #0f0f0f">
        <caption style="color: #0f0f0f;">Danh sách Sự Kiện: </caption>
        <h4><a href="them-su-kien.php" style="color: #8e0e12">Thêm mới Sự Kiện</a></h4>
        <thead>
        <tr>
            <th>STT</th>
            <th>Thể loại sự kiện</th>
            <th>Tiêu Đề</th>
            <th>Ảnh</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php
        $stt= 1;
        $sql = "select * from sukiennoibat";
        $query = mysqli_query($connect, $sql);
        while ($data = mysqli_fetch_array($query)) {
            ?>
            <tr>
                <th scope="row"><?php echo $stt++ ?></th>
                <td> <?php echo $data["TheLoaiSk"] ?></td>
                <td><?php echo $data["TieuDe"] ?></td>

                <td><?php echo '<img src=' .$data['Images'].' style="width: 100px; height: 100px;"/> '; ?></td>

                <td><a href="chinh-sua-su-kien.php?id=<?php echo $data["IdSuKien"]; ?>">Chỉnh Sửa</a> -
                    <a href="xoa-su-kien.php?id=<?php echo $data["IdSuKien"]; ?>">Xóa</a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>