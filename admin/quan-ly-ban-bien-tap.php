<?php
//    session_start();
    require ("connection.php");

    if (!isset($_SESSION["username"])) {
        header("Location: login.php");
    } else {
        $name = $_SESSION["username"];
        $quyen = $_SESSION["QuyenUser"];
        $IdUser = $_SESSION["IdUser"];
//        print_r( 'Tk: ' . $name . ' , quyền:  ' . $quyen .  ' , id: '. $IdUser);
    }
?>

<script type="text/javascript" src="js/ajax.js"></script>

<div style="margin: 5px 5px 5px 10px;">
    <h3>Quản lý ban biên tập</h3>
    <table class="table" style="color: #0f0f0f;">
        <caption style="color: white">Danh sách ban biên tập </caption>
        <h4>
            <a href="index.php?p=thembanbientap" style="color: white">
                Thêm ban biên tập mới
            </a>
        </h4>
        <thead>
        <tr>
            <th>Số thứ tự</th>
            <th>Họ Tên</th>
            <th>Gmail</th>
            <th>SDT</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php
            $stt = 1;
            $sql = "SELECT * FROM banbientap";
            // Thực thi câu lệnh với biến sql từ file connection
            $query = mysqli_query($connect, $sql);
            while ($data = mysqli_fetch_array($query)) {
        ?>
        <tr>
            <th scope="row"><?php echo $stt ++ ?></th>
            <td><?php echo $data["HoTen"]?></td>
            <td><?php echo $data["Gmail"]?></td>
            <td><?php echo $data["SĐT"]?></td>
            <td>
                <div id="edit_bientap">
                        <span id="insert_tintuc">
                              <a href="index.php?p=editbanbientap&id=<?php echo $data["IdBienTap"]; ?> ">Sửa</a> -
                    <span id="delete">
                            <a href="xoa-ban-bien-tap.php?id=<?php echo $data["IdBienTap"]; ?> ">Xóa</a>
                </div>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>