<?php
require ("connection.php");
?>

<script src="js/ajax.js">
</script>

<div>
    <h3>Quản lý thành viên</h3>
    <table class="table" style="color: black">
        <caption style="color: white">Danh sách thành viên đã đăng ký</caption>
        <thead>
        <tr>
            <th>STT</th>
            <th>Họ tên</th>
            <th>Tên tài khoản</th>
            <th>Mật khẩu</th>
            <th>Email</th>
            <th>Ngày sinh</th>
            <th>Quyền</th>
            <th>Trạng thái</th>
            <th> </th>
        </tr>
        </thead>

        <tbody>
        <?php
        $display = 10;
        $stt = 1;
        if (isset($_GET['page']) && (int) $_GET['page']) {
            $page = $_GET['page'];
        } else {
            $sql_count = "SELECT COUNT(IdUser) FROM user";
            $res = mysqli_query($connect, $sql_count);
            $row = mysqli_fetch_array($res);
            $record = $row[0];
            if ($record > $display) {
                $page = ceil($record/$display);
            } else {
                $page = 1;
            }
        }

        $start = (isset($_GET['start']) && (int)$_GET['start']) ? $_GET['start'] : 0;

        $sql = "SELECT * FROM user LIMIT $start,$display";

        // Thực thi câu lệnh $sql với biến connect tù file connection
        $query = mysqli_query($connect, $sql);
        while( $data = mysqli_fetch_array($query)) {
            ?>

            <tr  >
                <th scope="row"><?php echo $stt++ ?></th>
                <td><?php echo $data["HoTen"]; ?></td>
                <td><?php echo $data["UserName"]; ?></td>

                <td><?php
                        echo $data["Password"];
                    ?></td>

                <td><?php echo $data["Email"]; ?></td>
                <td><?php echo $data["NgaySinh"]; ?></td>
                <td><?php
                    if ($data["QuyenUser"] == 1) {
                        echo "Manage";
                    } else if ($data["QuyenUser"] == 2) {
                        echo 'Admin';
                    } else if ($data["QuyenUser"] == 3){
                        echo 'Nhân viên CSKH';
                    }
                    ?>
                </td>
                <td><?php
                    if ($data["TrangThai"] == 0){
                        echo "Tài khoản bị khóa";
                    } else if ($data["TrangThai"] == 1) {
                        echo "";
                    }
                    ?></td>
                <td>
                    <div id="add_insert_delete">
                        <span id="insert_user">
                             <a href="index.php?p=editthanhvien&id=<?php echo $data["IdUser"]; ?> ">
                                 <?php
                                 if ($quyen == 3)
                                     echo 'Chỉnh sửa ';
                                 ?>
                             </a> </span> -
                        <span id="delete">
                             <a href="xoa-thanh-vien.php?id=<?php echo $data["IdUser"]; ?>">
                                  <?php
                                  if ($quyen == 3)
                                     echo 'Xóa ';
                                  ?>
                             </a></span> -
                        <span id="khoa">
                             <a href="khoa-thanh-vien.php?id=<?php echo $data["IdUser"]; ?>"> <?php
                                if ($data["TrangThai"] == 1){ echo 'Khóa';} ?></a></span>
                        <span id="mo_khoa">
                             <a href="mo-khoa-thanh-vien.php?id=<?php echo $data["IdUser"]; ?>"> <?php
                                if ($data["TrangThai"] == 0){ echo 'Mở Khóa';} ?></a></span>


                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="phantrang"  style=" display: block;color: red; background-color: white"><li>
            <?php
            if($page > 1) {
                $next = $start + $display ;
                $prev = $start - $display ;
                $current = ($start/$display) + 1;
//            echo   'page'. $page . '  start ' . $start . '   prev' .$prev . '   ' ;


                if($current != 1) {
                    echo "<a class='pagination' href='quan-ly-thanh-vien.php?start=$prev'>Previous - </a>";
                }
                // hiển thị số link
                for( $i = 1; $i < $page ; $i++) {
                    echo "<a class='pagination' href='quan-ly-thanh-vien.php?start=".($display*($i-1))."'>  Trang $i - </a>";
                }

                // Hiển thị trang nexxt
                if( $current != $page) {
                    echo "<a class='pagination' href='quan-ly-thanh-vien.php?start=$next'>Next</a>";
                }
            }
            ?>
        </li></ul>

</div>