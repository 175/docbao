<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng ký thành viên</title>

    <link rel="stylesheet" type="text/css" href="css/css.css"/>
    <script type="text/javascript" rel="script" href="js/ajax.js"></script>

</head>
<body id="body_register">

    <?php
        require_once ("connection.php");

        if (isset($_POST["btn_register_submit"])) {
            // lấy thông tin từ form = phương thức post
            $hoten = $_POST["hoten"];
            $username = $_POST["username"];
            $password = $_POST["password"];
            $email = $_POST["email"];
            $ngaysinh = $_POST["ngaysinh"];
//            if ($_POST["QuyenUser"] == "Manage") {
//                $quyen = 1;
//            } else if ($_POST["QuyenUser"] == "Admin") {
//                $quyen = 2;
//            } else if ($_POST["QuyenUser"] == "CSKH" ) {
//                $quyen = 3;
//            }

             $quyen =  $_POST["QuyenUser"];
//             exit($quyen);

            // Kiểm tra điều kiện bắt buộc cho các trường ko được bỏ trống
            if ($username == "" || $password == "" || $hoten == "" || $email == ""|| $ngaysinh == "" || $quyen == ""){
                echo 'Vui lòng nhập đầy đủ thông tin';
            } else {
                // Kiểm tra tài khoản đã tồn tại hay chưa
                $sql = "select * from user WHERE UserName = '$username'";
                $kt = mysqli_query($connect, $sql);

                if (mysqli_num_rows($kt) > 0) {
                    echo "Thong tin tai khoan da ton tai";
                } else {

                    // thực hiện lưu trữ dữ liệu vào db
                   echo '<br>';
//                     $sql = "INSERT INTO user
//                          (IdUser, HoTen, UserName,
//                           Password, Email, NgaySinh, QuyenUser
//                    ) VALUES
//                    ('4', 'Thanh Thủy', 'admin', '123',
//                    'abc@gmail.com', '0000-00-00', 1) ";

/*
 *                   INSERT INTO `user` ( `HoTen`, `UserName`, `Password`,
 *                       `Email`, `NgaySinh`, `QuyenUser`) VALUES
 *                      ('4', '4', '4', '4', '2017-06-02', '1')
 */
                    $sql = "INSERT INTO user(
	    					HoTen, UserName, Password,
						    Email, 	NgaySinh, QuyenUser
	    					) VALUES (
	    					'$hoten',
	    					'$username',
						    '$password',
	    					'$email',
	    					'$ngaysinh',
	    					'$quyen'
	    					)";
//                    exit($sql);

                    if(mysqli_query($connect, $sql)) {
//                        exit(mysqli_query($connect, $sql));
                        echo 'Chúc mừng bạn đã đăng ký thành công';
                        echo '<br>';
                        echo "Nhấn vào đây để <a href='login.php'>Đăng nhập</a>";
                        header("Location login.php");
                    } else {
//                        exit(mysqli_query($connect, $sql));
                       echo 'Thêm thất bại. Xin vui lòng kiểm tra lại';
                    }
                }
            }
        }
    ?>


<div id="main_register" >
    <table id="tbl_register" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#999999">
    <tr>
        <form action="register.php" method="post" name="form1">
            <td>
                <table width="150%" border="0" cellpadding="3" cellspacing="1" bgcolor="#f0f8ff " >
                    <tr><td colspan="3"> <label  id="label_message" ></td></tr>
                    <tr><td colspan="2"><strong>Đăng ký thành viên</strong></td> </tr>
                    <tr><td colspan="3"> <label  id="label_message" ></td></tr>
                    <tr>
                        <td>Họ tên:</td>
                        <td> <input type="text"  name="hoten"/></td>
                    </tr>
                    <tr>
                        <td>Username: </td>
                        <td><input type="text" name="username" id="login"/></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password"  name="password"/> </td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><input type="text" name="email" /></td>
                    </tr>
                    <tr>
                        <td>Ngay Sinh:</td>
                        <td><input type="date" name="ngaysinh"  /></td>
                    </tr>
                    <tr><td>Quyền: </td> <td>
                            <select name="QuyenUser">
                                <option value="1" >Manage</option>
                                <option value="2" >Admin</option>
                                <option value="3" >CSKH</option>
                            </select>
                        </td></tr>
                    <tr><td colspan="3"> <label  id="label_message" ></td></tr>
                    <tr>
                        <td colspan="2" align="center">
      <input id="doregister" type="submit" name="btn_register_submit" value="Đăng ký" onclick="responsivePHP()></td>
                    </tr>
                    <tr> <td colspan="3"> <label  id="label_message" ></td></tr>

                </table>
            </td>
        </form>

    </tr>
    </table>
</div>

</body>
</html>