<div class="news_top">
    <div class="news_top_hd">
        <span class="time"></span>
        <div class="news_top_tabs">
            <a id="news-top-tabs-1" href="javascript:void(0);" class="on">Tin nổi bật
            </a> <a style="background-color: #01BBF2; color: #ffffff" id="news-top-tabs-2" href="javascript:void(0);">Sự
                kiện nổi bật&nbsp;
                <img src="images/icon_hot_1.gif" height="10px"></a>
        </div>
    </div>
    <div class="news_top_ct" id="news-top-ct-1" style="display: block;">
        <div class="ct_left">
            <?php
            $tintuc_sukien = getTinNoiBat_DauTrang();
            $i = 0;
            while ($tintuc_sukien_show = mysqli_fetch_assoc($tintuc_sukien)) {
                ;
                ?>
                <div id="itm-news0" class="news_item" style="display: none;">

                    <a class="photo"
                       href="/tin-tuc/27-06-2017/me-phuong-nga-co-bang-chung-trao-doi-voi-nguoi-bi-an/33/463343">
                        <div class="crop">
                            <img src="http://media.docbao.vn/\files\images\site-1\20170627/me-phuong-nga-co-bang-chung-trao-doi-voi-nguoi-bi-an-33-27062017163241-400x300.jpg"
                                 alt="Mẹ Phương Nga có bằng chứng trao đổi với &quot;người bí ẩn&quot;" width="400">
                        </div>
                    </a>
                    <div class="text">
                        <h4>
                            <a href="#">Mẹ Phương Nga có bằng chứng trao đổi với "người bí ẩn"</a></h4>
                        <p>Bà Hồ Mai Phương không có tư cách tố tụng nhưng được mời lên làm nhân chứng.</p>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
        <div class="ct_right">
            <ul class="news_lst">
                <?php
                $tintuc_sukien = getTinNoiBat_DauTrang();
                $i = 0;
                while ($tintuc_sukien_show = mysqli_fetch_assoc($tintuc_sukien)) {
                    ?>
                    <li><a onmouseover="Slidenews(<?php $i++ ?>)" href="#">Mẹ Phương Nga có bằng chứng trao đổi với "người bí ẩn"</a></li>
                    <?php
                }
                ?>
                <div style="font-weight: bold; line-height: 35px; display:none">Tin doanh nghiệp</div>
                <li style="display:none"><a href="#">FE CREDIT ký kết hợp đồng hợp vốn 100 triệu USD với ngân hàng
                        Credit Suisse</a></li>

                <!--  <div style="font-weight: bold; line-height: 35px;">Tin doanh nghiệp</div>
                  <li><a href="http://docbao.vn/tin-tuc/23-05-2016/cuoc-song-ly-tuong-tai-noc-nha-ha-dong-hp-landmark-tower/65/363890">Cuộc sống lý tưởng tại Nóc nhà Hà Đông HP Landmark Tower</a></li>
             -->
            </ul>

        </div>
        <div class="clrb">
        </div>

    </div>

    <script language="javascript" type="text/javascript">
        var curPos1 = 0;
        var timer1 = setTimeout("Slidenews(0)", 9000);

        function Slidenews(index1) {
            clearTimeout(timer1);

            document.getElementById("itm-news" + curPos1).style.display = "none";
            document.getElementById("itm-news" + index1).style.display = "";

            curPos1 = index1;
            index1 = (index1 == 9) ? 0 : (index1 + 1);
            timer1 = setTimeout("Slidenews(" + index1 + ")", 9000);
        }
    </script>

    <div style="display: none;" class="news_top_ct" id="news-top-ct-2">
        <div class="ct_left">
            <div id="itm-news-hot0" class="news_item" style="">
                <a class="photo" href="#">
                    <div class="crop">
                        <img src="http://media.docbao.vn/\files\images\site-1\20170627/me-phuong-nga-co-bang-chung-trao-doi-voi-nguoi-bi-an-33-27062017163241-400x300.jpg"
                             alt="Mẹ Phương Nga có bằng chứng trao đổi với &quot;người bí ẩn&quot;" width="400">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Mẹ Phương Nga có bằng chứng trao đổi với "người bí ẩn"</a></h4>
                    <p>Bà Hồ Mai Phương không có tư cách tố tụng nhưng được mời lên làm nhân chứng.</p>
                </div>
            </div>
            <div id="itm-news-hot1" class="news_item" style="display: none;">
                <a class="photo" href="#">
                    <div class="crop">
                        <img src="http://media.docbao.vn/\files\images\site-1\20170623/gia-vang-mieng-nguoc-chieu-the-gioi-30-23062017094350-400x300.jpg"
                             alt="Giá vàng miếng ngược chiều thế giới" width="400">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Giá vàng miếng ngược chiều thế giới</a></h4>
                    <p>Giá mỗi lượng giảm vài chục nghìn đồng so với hôm qua ở một số nơi, trong bối cảnh giá thế giới
                        tăng 2 phiên liên tiếp.</p>
                </div>
            </div>
            <div id="itm-news-hot2" class="news_item" style="display: none;">
                <a class="photo" href="#">
                    <div class="crop">
                        <img src="http://media.docbao.vn/\files\images\site-1\20170627/ky-thi-thpt-quoc-gia-2017-cac-mon-tu-nhien-se-nhieu-diem-10-29-27062017065544-400x300.jpg"
                             alt="Kỳ thi THPT quốc gia 2017: Các môn tự nhiên sẽ nhiều điểm 10" width="400">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Kỳ thi THPT quốc gia 2017: Các môn tự nhiên sẽ nhiều điểm 10</a></h4>
                    <p>Theo đánh giá của nhiều giáo viên, học sinh, do đề thi năm nay tương đối “dễ thở” nên điểm sàn và
                        điểm chuẩn xét tuyển vào các trường ĐH, CĐ dự kiến sẽ tăng so với các năm trước.</p>
                </div>
            </div>
            <div id="itm-news-hot3" class="news_item" style="display: none;">
                <a class="photo" href="#">
                    <div class="crop">
                        <img src="http://media.docbao.vn/\files\images\site-1\20170627/ong-trump-thang-lon-ve-lenh-cam-nhap-canh-35-27062017082900-400x300.jpg"
                             alt="Ông Trump thắng lớn về lệnh cấm nhập cảnh" width="400">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Ông Trump thắng lớn về lệnh cấm nhập cảnh</a></h4>
                    <p>Tòa Tối cao Mỹ vừa trao cho Tổng thống Donald Trump một chiến thắng khi giảm một phần mức độ phán
                        quyết vô hiệu hóa lệnh cấm nhập cảnh 6 nước đông dân Hồi giáo mà tòa án cấp dưới đã đưa ra.</p>
                </div>
            </div>
            <div id="itm-news-hot4" class="news_item" style="display: none;">
                <a class="photo" href="#">
                    <div class="crop">
                        <img src="http://media.docbao.vn/\files\images\site-1\20170627/thuong-vu-dat-gia-nhat-trong-lich-su-cua-chinese-super-league-25-27062017112048-400x300.jpg"
                             alt="Thương vụ đắt giá nhất trong lịch sử của Chinese Super League" width="400">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Thương vụ đắt giá nhất trong lịch sử của Chinese Super League</a></h4>
                    <p>Với 42 triệu euro, Quảng Châu Hằng Đại biến Jackson Martinez trở thành thương vụ đắt giá nhất
                        trong lịch sử Chinese Super League. Cũng bằng số tiền khổng lồ ấy, giải đấu Trung Quốc khiến cả
                        thế giới phải nể phục về độ ‘chịu chi và chịu chơi’.</p>
                </div>
            </div>
            <div id="itm-news-hot5" class="news_item" style="display: none;">
                <a class="photo" href="#1">
                    <div class="crop">
                        <img src="http://media.docbao.vn/\files\images\site-1\20170626/soc-hon-ca-vu-moi-tham-minh-tu-mang-lan-khue-coi-cai-quan-don-mong-ra-31-26062017175042-400x300.png"
                             alt="Sốc hơn cả vụ &quot;môi thâm&quot;, Minh Tú mắng Lan Khuê: &quot;Cởi cái quần độn mông ra&quot;"
                             width="400">
                    </div>
                </a>
                <div class="text">
                    <h4>
                        <a href="#">Sốc hơn cả vụ "môi thâm", Minh Tú mắng Lan Khuê: "Cởi cái quần độn mông ra"</a></h4>
                    <p>Không chỉ mắng Lan Khuê là "môi thâm", Minh Tú còn gây sốc cho khán giả khi yêu cầu nữ đồng
                        nghiệp phải "cởi cái quần độn mông ra".</p>
                </div>
            </div>
        </div>
        <div class="ct_right">
            <ul class="news_lst">
                <li><a onmouseover="Slidenewshot(0)" href="#"><strong>Hoa hậu Trương Hồ Phương Nga hầu tòa với cáo buộc
                            lừa 16,5 tỷ đồng</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(0)" style="font-weight: normal;">Mẹ Phương Nga có bằng chứng
                        trao đổi với "người bí ẩn"</a></li>
                <li><a onmouseover="Slidenewshot(1)" href="#"><strong>Khóc cười với giá vàng</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(1)" style="font-weight: normal;">Giá vàng miếng ngược chiều
                        thế giới</a></li>
                <li><a onmouseover="Slidenewshot(2)" href="#"><strong>Mùa thi tốt nghiệp, tuyển sinh ĐH
                            2017</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(2)" style="font-weight: normal;">Kỳ thi THPT quốc gia 2017:
                        Các môn tự nhiên sẽ nhiều điểm 10</a></li>
                <li><a onmouseover="Slidenewshot(3)" href="#"><strong>TT Donald Trump ký sắc lệnh cấm nhập cư gây tranh
                            cãi</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(3)" style="font-weight: normal;">Ông Trump thắng lớn về lệnh
                        cấm nhập cảnh</a></li>
                <li><a onmouseover="Slidenewshot(4)" href="#"><strong>Thông tin chuyển nhượng</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(4)" style="font-weight: normal;">Thương vụ đắt giá nhất trong
                        lịch sử của Chinese Super League</a></li>
                <li><a onmouseover="Slidenewshot(5)" href="#"><strong>The Face 2017</strong></a><br>
                    <a href="#" onmouseover="Slidenewshot(5)" style="font-weight: normal;">Sốc hơn cả vụ "môi thâm",
                        Minh Tú mắng Lan Khuê: "Cởi cái quần độn mông ra"</a></li>
            </ul>
        </div>
        <div class="clrb">
        </div>
    </div>


    <script language="javascript" type="text/javascript">
        var curPos2 = 0;
        var timer2 = setTimeout("Slidenewshot(1)", 9000);

        function Slidenewshot(index2) {
            clearTimeout(timer2);

            document.getElementById("itm-news-hot" + curPos2).style.display = "none";
            document.getElementById("itm-news-hot" + index2).style.display = "";

            curPos2 = index2;
            index2 = (index2 == 5) ? 0 : (index2 + 1);
            timer2 = setTimeout("Slidenewshot(" + index2 + ")", 9000);
        }
    </script>

</div>