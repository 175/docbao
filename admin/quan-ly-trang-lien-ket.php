<?php require_once ("connection.php"); ?>

<div>
    <h3> Quản lý Liên Kết: </h3>
    <table class="table" style="color: #0f0f0f">
        <caption style="color: white;">Danh sách Liên Kết: </caption>
        <h4><a href="index.php?p=themtranglk" style="color: white">Thêm mới Liên Kết</a></h4>
        <thead>
        <tr>
            <th>STT</th>
            <th>Tên Trang Liên Kết</th>
            <th>Link</th>
            <th>Tiêu Đề</th>
            <th>Ghi chú</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php
        $stt= 1;
        $sql = "select * from tranglienket";
        $query = mysqli_query($connect, $sql);
        while ($data = mysqli_fetch_array($query)) {
            ?>
            <tr>
                <th scope="row"><?php echo $stt++ ?></th>
                <td> <?php echo $data["TenTrangLK"] ?></td>
                <td><?php echo $data["UrlTrangLK"] ?></td>
                <td><?php echo $data["TitleTrangLK"] ?></td>
                <td><?php echo $data["GhichuTrangLK"] ?></td>
                <td><a href="index.php?p=editlienket&id=<?php echo $data["IdTrangLK"]; ?>">Chỉnh Sửa</a> -
                    <a href="xoa-lien-ket.php?id=<?php echo $data["IdTrangLK"]; ?>">Xóa</a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>