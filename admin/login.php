<?php
    session_start();
    if ( isset($_SESSION['username'] )) {
        header("Location: index.php");
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="css/css.css"/>

    <title>Đăng nhập</title>
</head>
<body id="body_login">

<div id="main_login">
    <table id="table_login" align="center" cellpadding="10" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
        <form name="form1" method="post" action="checklogin.php" >
            <td>
                <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#ffffff">
                    <tr>
                        <td colspan="3"><strong> Đăng nhập hệ thống  </strong></td>
                    </tr>

                    <tr>
                        <td width="30px">Username</td>
                        <td><input name="username" type="text" id="username" placeholder="ten tai khoan" size="30"/></td>
                    </tr>

                    <tr>
                        <td width="30px">Password</td>
                        <td><input name="password" type="password" id="password" placeholder="mat khau" size="30"/></td>
                    </tr>

                    <tr>
                        <td colspan="2" align="center" width="20px"><input type="submit" name="btn_login_submit" value="Đăng nhập" /></td>
                    </tr>

                </table>
            </td>
        </form>

    </tr>
    </table>
</div>
</body>
</html>