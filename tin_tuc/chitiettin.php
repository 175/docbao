<?php
if (isset($_GET["idTinTuc"])) {
    $idTin = $_GET["idTinTuc"];
    settype($idTin, "int");
} else {
    $idTin = 1;
}
?>
<?php
if (isset($_GET["idTL"])) {
    $idTL = $_GET["idTL"];
    settype($idTL, "int");
} else {
    $idTL = 1;
}
?>
<?php
UpDateSoLanXem($idTin); ?>
<?php
$tin = getChiTietTin($idTin);
$row_tin = mysqli_fetch_assoc($tin);
?>
<div class="col_left">
    <div class="detail_page">
        <h5 class="forum_name">
            <a style="text-decoration: none; color: #00bbf3;"
               href="index.php?p=tintrongtheloai&idTL=<?php echo $row_tin['idTheLoai'] ?>">

                <?php echo $row_tin["TenTheLoai"] ?>
            </a>
            <a style="text-decoration: none; color: red;"
               href="index.php?p=tintrongtheloai&idTinTuc=<?php echo $row_tin['idTinTuc'] ?>">
            </a>
        </h5>
        <div class="detail_top">
            <h1>
                <span><?php echo $row_tin["TieuDe"] ?></span></h1>

            <!--            like, share face phia trên -->
            <div style="height: 30px; overflow: hidden;" class="like_box">
                <div class="social_network">
                    <div class="fb-like fb_iframe_widget"
                         data-href="http://docbao.vn/tin-tuc/chi-tiet-bai-viet-id-462481" data-layout="button_count"
                         data-action="like" data-show-faces="true" data-share="false" fb-xfbml-state="rendered"
                         fb-iframe-plugin-query="action=like&amp;app_id=382248452171272&amp;container_width=0&amp;href=http%3A%2F%2Fdocbao.vn%2Ftin-tuc%2Fchi-tiet-bai-viet-id-462481&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=false&amp;show_faces=true"
                         style="
                                                                        float: left;
                                                                        text-align: right;
                                                                    ">
                        <span style="vertical-align: bottom; width: 68px; height: 20px;">
                            <iframe name="f142334727825cc"
                                    width="1000px"
                                    height="1000px"
                                    frameborder="0"
                                    allowtransparency="true"
                                    allowfullscreen="true"
                                    scrolling="no"
                                    title="fb:like Facebook Social Plugin"
                                    src="https://www.facebook.com/plugins/like.php?action=like&amp;app_id=1696642980585812&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df2f1cec6c9e8804%26domain%3Ddocbao.vn%26origin%3Dhttp%253A%252F%252Fdocbao.vn%252Ff1691d044ffdae8%26relation%3Dparent.parent&amp;container_width=0&amp;href=http%3A%2F%2Fdocbao.vn%2Ftin-tuc%2Fchi-tiet-bai-viet-id-462481&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=false&amp;show_faces=true"
                                    style="border: none; visibility: visible; width: 68px; height: 20px;"
                                    class=""></iframe></span>
                    </div>
                    <div class="fb-share-button fb_iframe_widget" style="float: left"
                         data-href="http://docbao.vn/tin-tuc/chi-tiet-bai-viet-id-462481" data-type="button_count"
                         fb-xfbml-state="rendered"
                         fb-iframe-plugin-query="app_id=382248452171272&amp;container_width=0&amp;href=http%3A%2F%2Fdocbao.vn%2Ftin-tuc%2Fchi-tiet-bai-viet-id-462481&amp;locale=vi_VN&amp;sdk=joey&amp;type=button_count">
                        <span style="vertical-align: bottom; width: 77px; height: 20px;">
                            <iframe name="f1d81d2d5c97754"
                                    width="1000px"
                                    height="1000px"
                                    frameborder="0"
                                    allowtransparency="true"
                                    allowfullscreen="true"
                                    scrolling="no"
                                    title="fb:share_button Facebook Social Plugin"
                                    src="https://www.facebook.com/plugins/share_button.php?app_id=1696642980585812&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df34e4379e4a056c%26domain%3Ddocbao.vn%26origin%3Dhttp%253A%252F%252Fdocbao.vn%252Ff1691d044ffdae8%26relation%3Dparent.parent&amp;container_width=0&amp;href=http%3A%2F%2Fdocbao.vn%2Ftin-tuc%2Fchi-tiet-bai-viet-id-462481&amp;locale=vi_VN&amp;sdk=joey&amp;type=button_count"
                                    style="border: none; visibility: visible; width: 77px; height: 20px;"
                                    class=""></iframe></span>
                    </div>
                </div>
                <span class="time">
                        <span><?php echo $row_tin["TimeDangBai"] ?></span>
                        GMT+7</span>
                <div class="clrb">
                </div>
            </div>
            <!--          hết  like, share face phia trên -->

        </div>

        <div class="detail_content">
            <p style="text-align: justify;"><strong><span style="color:#000000">
                        <?php echo $row_tin["TomTat"] ?></span></strong></p>

            <div>
                <?php
                echo $row_tin["NoiDung"];
                ?>
            </div>

            <div>
                <p style="text-align: right;"><strong><span style="color:#000000">
                            Theo <?php echo $row_tin["HoTen"] ?> (<?php echo $row_tin["NguonTin"] ?>) </span></strong>
                </p>
            </div>

        </div>

<!--        Bình luận face-->
        <div class="fb-comments" data-href="http://localhost:8888/docbao/index.php?p=chitiettin&idLT=<?php echo $row_tin['idTheLoai'] ?>"
             data-width="680"
             data-numposts="5"></div>
<!--        Hết bình luận của face-->


        <p class="the-article-source-url">
            <img width="16px" style="display: block; float: left" src="images/tintuc/link_icon_65.png">
            <a rel="nofollow" target="_blank" title="Xem bài gốc" href=""></a>
        </p>
        <!-- Hien thi quan cao itvc -->
        <div id="itvcplayer">
            <p></p>
            <p>
            </p></div>

        <br style="clear:both">

        <br style="clear">

        <br style="clear:both">
        <table cellspacing="5" cellpadding="5" border="0" width="650px">
            <tbody>
            <tr>
                <td>
                    <?php
                    $tin_view_max = getTinDocNhieu_motTin();
                    $row_tin = mysqli_fetch_array($tin_view_max);
                    ?>
                    <div id="text_live">
                        <div class="block_image_news width_common">
                            <div class="thumb">
                                <a href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>"
                                   target="_blank">
                                    <img style="width: 100%;" alt="<?php echo $row_tin["TieuDe"] ?>"
                                         title="<?php echo $row_tin["TieuDe"] ?>"
                                         src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>">
                                </a>
                            </div>
                            <div class="title_news"><a class="txt_link"
                                                       href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>"
                                                       target="_blank"><strong>
                                        <?php echo $row_tin["TieuDe"] ?></strong></a></div>
                            <div data-mobile-href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>"
                                 class="news_lead"><span><span>
                                        <?php echo $row_tin["TomTat"] ?></span></span></div>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <!--        like, share của face phía dưới -->

        <div class="detail_top">
            <div style="height: 60px; overflow: hidden;" class="like_box">
                <div style="width: 660px;" class="social_network">
                    <!--                    <div class="fb-like fb_iframe_widget"-->
                    <!--                         data-href="http://localhost:8888/docbao/index.php?p=chitiettin&idTinTuc=-->
                    <?php //echo $row_tin['IdTinTuc'] ?><!--"-->
                    <!--                         data-layout="button_count"-->
                    <!--                         data-action="like" data-show-faces="true" data-share="false" fb-xfbml-state="rendered"-->
                    <!--                         fb-iframe-plugin-query="action=like&amp;app_id=382248452171272&amp;container_width=0&amp;-->
                    <!--                            href=http%3A%2F%2Fhttp://localhost:8888/docbao/index.php%2Fp=chitiettin%2FidTinTuc=-->
                    <?php //echo $row_tin['IdTinTuc'] ?><!--&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=false&amp;show_faces=true"-->
                    <!--                         style="-->
                    <!--                                                                        float: left;-->
                    <!--                                                                        text-align: right;-->
                    <!--                                                                    ">-->
                    <!--                        <span style="vertical-align: bottom; width: 68px; height: 20px;">-->
                    <!--                            <iframe name="f142334727825cc"-->
                    <!--                                    width="1000px"-->
                    <!--                                    height="1000px"-->
                    <!--                                    frameborder="0"-->
                    <!--                                    allowtransparency="true"-->
                    <!--                                    allowfullscreen="true"-->
                    <!--                                    scrolling="no"-->
                    <!--                                    title="fb:like Facebook Social Plugin"-->
                    <!--                                    src="https://www.facebook.com/plugins/like.php?action=like&amp;app_id=382248452171272&amp;-->
                    <!--                                        channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df2f1cec6c9e8804%26domain%3Dhttp://localhost:8888/docbao/index.php%26origin%3Dhttp%253A%252F%252Fhttp://localhost:8888/docbao/index.php%252Ff1691d044ffdae8%26relation%3Dparent.parent&amp;container_width=0&amp;-->
                    <!--                                        href=http%3A%2F%2Fhttp://localhost:8888/docbao/index.php%2Fp=chitiettin%2FidTinTuc=-->
                    <?php //echo $row_tin['IdTinTuc'] ?><!--&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=false&amp;show_faces=true"-->
                    <!--                                    style="border: none; visibility: visible; width: 68px; height: 20px;"-->
                    <!--                                    class=""></iframe></span>-->
                    <!--                    </div>-->
                    <!--                    <div class="fb-share-button fb_iframe_widget" style="float: left"-->
                    <!--                         data-href="http://localhost:8888/docbao/index.php?p=chitiettin&idTinTuc=-->
                    <?php //echo $row_tin['IdTinTuc'] ?><!--"-->
                    <!--                         data-type="button_count"-->
                    <!--                         fb-xfbml-state="rendered"-->
                    <!--                         fb-iframe-plugin-query="app_id=382248452171272&amp;container_width=0&amp;-->
                    <!--                                   href=http%3A%2F%2Fhttp://localhost:8888/docbao/index.php%2Fp=chitiettin%2FidTinTuc=-->
                    <?php //echo $row_tin['IdTinTuc'] ?><!--&amp;locale=vi_VN&amp;sdk=joey&amp;type=button_count">-->
                    <!--                        <span style="vertical-align: bottom; width: 77px; height: 20px;">-->
                    <!--                            <iframe name="f1d81d2d5c97754"-->
                    <!--                                    width="1000px"-->
                    <!--                                    height="1000px"-->
                    <!--                                    frameborder="0"-->
                    <!--                                    allowtransparency="true"-->
                    <!--                                    allowfullscreen="true"-->
                    <!--                                    scrolling="no"-->
                    <!--                                    title="fb:share_button Facebook Social Plugin"-->
                    <!--                                    src="https://www.facebook.com/plugins/share_button.php?app_id=382248452171272&amp;-->
                    <!--                                                                    channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df34e4379e4a056c%26domain%3Dhttp://localhost:8888/docbao/index.php%26origin%3Dhttp%253A%252F%252Fhttp://localhost:8888/docbao/index.php%252Ff1691d044ffdae8%26relation%3Dparent.parent&amp;container_width=0&amp;-->
                    <!--                                                                    href=http%3A%2F%2Fhttp://localhost:8888/docbao/index.php%2Fp=chitiettin%2FidTinTuc=-->
                    <!--                            <?php //echo $row_tin['IdTinTuc'] ?><!--&amp;locale=vi_VN&amp;sdk=joey&amp;type=button_count"-->
                    <!--                                    style="border: none; visibility: visible; width: 77px; height: 20px;"-->
                    <!--                                    class="">-->
                    <!--                                                        </iframe>-->
                    <!--                            <!--                            <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=111&layout=button&action=like&size=small&show_faces=true&share=true&height=65&appId=382248452171272"-->
                    <!--                            <!--                                    width="111" height="65" style="border:none;overflow:hidden" scrolling="no"-->
                    <!--                            <!--                                    frameborder="0" allowTransparency="true">-->
                    <!--                            <!--                                -->
                    <!--                            <!--                            </iframe>-->
                    <!--                        </span>-->
                    <!--                    </div>-->

                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=382248452171272";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                    <div class="fb-like"
                         data-href="http://localhost:8888/docbao/index.php?p=chitiettin&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>"
                         data-layout="button"
                         data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>

                    <div style="float: right;">
                        <div data-show-border="false" data-stream="false" data-header="true" data-show-faces="false"
                             data-width="268" data-href="https://www.facebook.com/docbao.vn"
                             style="height: 60px; overflow: hidden;" class="fb-like-box fb_iframe_widget"
                             fb-xfbml-state="rendered"
                             fb-iframe-plugin-query="app_id=1696642980585812&amp;container_width=268&amp;header=true&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhttp://localhost:8888/docbao/index.php&amp;locale=vi_VN&amp;sdk=joey&amp;show_border=false&amp;show_faces=false&amp;stream=false&amp;width=268">
                            <span style="vertical-align: bottom; width: 268px; height: 70px;">
                                <iframe
                                        src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdocbao.vn%2F&tabs=timeline&width=278&height=50&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=382248452171272"
                                        width="278" height="60" style="border:none;overflow:hidden" scrolling="no"
                                        frameborder="0" allowTransparency="true">
                                </iframe>
                            </span>
                        </div>
                    </div>

                    <br clear="all">
                </div>
                <div class="clrb">
                </div>
            </div>
            <div id="fb-root"></div>

        </div>
        <!---->

        <!--        hết like, share của face phía dưới -->

        <div id="adContainer">
            <div id="adPlayer">
            </div>
        </div>
        <!-- END -->


        <div style="clear:both"></div>
        <!-- End code 336x280 -->
        <br>


        <div class="detail_page">
            <h5 class="forum_name">Tin gần đây</h5>
            <div class="related_news">
                <ul>

                    <?php
                    $tin = getTinGanDay_4tin($idTL);
                    while ($row_tin = mysqli_fetch_array($tin)) {
                        ?>
                        <li style="margin-bottom: 5px;">
                            <a class="photo"
                               href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                                <img width="165" height="124"
                                     src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>"
                                     alt="<?php echo $row_tin['TieuDe'] ?>"></a>
                            <h6>
                                <a href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                                    <?php echo $row_tin['TieuDe'] ?>

                                    <span style="font: 11px;"><i>(20/06)</i></span></a></h6>
                        </li>
                        <?php
                    }
                    ?>

                </ul>
                <div class="clrb">
                </div>
            </div>
        </div>


    </div>

    <!-- qc noi dung phu hop -->

    <!-- end qc -->

    <div class="general_news">


        <div style="width: 100%" class="news_box">
            <div class="detail_page">
                <h5 class="forum_name">Tin mới hơn</h5>
                <ul class="icon-list">

                    <?php
                    $tin = getTinMoiHon_4tin($idTL);
                    while ($row_tin = mysqli_fetch_array($tin)) {
                        ?>
                        <li><a style="font-size:11pt;"
                               href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                                <?php echo $row_tin['TieuDe'] ?>
                                <span><i>- (<?php echo $row_tin['TimeDangBai'] ?>)</i></span></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>


        <br class="clear">
        <!-- Quang cao -->
        <div class="advbox" style="margin:10px 18px;">

        </div>


        <div style="width: 100%" class="news_box">
            <div class="detail_page">
                <h5 class="forum_name">Tin cũ hơn</h5>
                <ul class="icon-list">
                    <?php
                    $tin = getTinCuHon_4tin($idTL);
                    while ($row_tin = mysqli_fetch_array($tin)) {
                    ?>
                    <li><a style="font-size:11pt;"
                           href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                            <?php echo $row_tin['TieuDe'] ?>
                            <span><i>- (<?php echo $row_tin['TimeDangBai'] ?>)</i></span></a></li>
                </ul>
                <?php
                }
                ?>
            </div>
        </div>


        <br class="clear">
    </div>
</div>