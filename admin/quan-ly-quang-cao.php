<div>
    <link rel="stylesheet" type="text/css" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="js/ajax.js"></script>
    <link rel="stylesheet" type="text/css" href="css/css.css">

    <?php
    require("connection.php");
    ?>

    <div>
        <h3>Quản lý quảng cáo</h3>
        <table class="table" style="color: #0f0f0f;" >



            <h4><a href="index.php?p=themquangcao" style="color: white" >
                    Thêm mới quảng cáo</a></h4>
            <thead >
            <tr>
                <th>STT</th>
                <th>Mô tả quảng cáo</th>
                <th>Link </th>
                <th>Hình ảnh</th>
                <th>Vi trí</th>
                <th> </th>
            </tr>
            </thead>

            <tbody>
            <?php
            $display = 10;
            $stt = 1;
            if (isset($_GET['page']) && (int) $_GET['page']) {
                $page = $_GET['page'];
            } else {
                $sql_count = "SELECT COUNT(IdQuangCao) FROM quangcao";
                $res =  mysqli_query($connect, $sql_count) or die('could not find the loai'. mysqli_error());
                $row = mysqli_fetch_array($res, MYSQLI_NUM);
                $record = $row[0];
                if ($record > $display) {
                    $page = ceil($record/$display);
                } else {
                    $page = 1;
                }
            }

            $start = (isset($_GET['start']) && (int)$_GET['start']) ? $_GET['start'] : 0;

            $sql = "SELECT * FROM quangcao LIMIT $start, $display";

            // Thực thi câu lệnh $sql với biến connect tù file connection
            $query = mysqli_query($connect, $sql) or die('could connect quangcao' . mysqli_error());

            while( $data = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                ?>
                <tr  >
                    <th scope="row"><?php echo $data["IdQuangCao"]; ?></th>
                    <td><?php echo $data["MoTaQC"]; ?></td>
                    <td><?php echo $data["UrlQuangCao"]; ?></td>

                    <td>
                        <?php echo '<img src=' . '../images/' . $data['UrlImages'] . ' style="width: 50px; height: 50px;"/> '; ?>
                    </td>

                    <td><?php echo $data["Vitri"]; ?></td>


                    <td>
                        <div id="add_insert_delete">
                        <span id="">
                             <a class="#" href="index.php?p=chinhsuaquangcao&id=<?php echo $data["IdQuangCao"]; ?>">Chỉnh sửa </a> </span> -
                            <span id="">
                             <a class="deletetheloai" href="xoa-quang-cao.php?id=<?php echo $data["IdQuangCao"]; ?>">Xóa</a></span>
                        </div>
                    </td>
                </tr>
                <?php
            }

            ?>
            </tbody>
        </table>
        <ul class="phantrang" style=" display: block;color: red; background-color: white"><li>
                <?php

                if($page > 1) {
                    $next = $start + $display ;
                    $prev = $start - $display ;
                    $current = ($start/$display) + 1;
                    if($current != 1) {
                        echo "<a class='pagination' href='quan-ly-quang-cao.php?start=$prev'>Previous - </a>";
                    }
                    // hiển thị số link
                    for( $i = 1; $i < $page ; $i++) {
                        echo "<a class='pagination' href='quan-ly-quang-cao.php?start=".($display*($i-1))."'>  Trang $i - </a>";
                    }

                    // Hiển thị trang nexxt
                    if( $current != $page) {
                        echo "<a class='pagination' href='quan-ly-quang-cao.php?start=$next'>Next</a>";
                    }
                }
                ?>
            </li></ul>

    </div>


</div>