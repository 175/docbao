<?php




    function UploadFile($name, $folder, $max, $type, $tmp_name)
    {

        $uploadOK = 0;
        $targetfile = $folder . $name;   // lấy ra tên file có thư mục ở đầu : images/hinhnen.jpg
        $imageFileType = pathinfo($targetfile, PATHINFO_EXTENSION);     // lấy ra tên đuôi của ảnh : jpeg, jpg, png, gif ..

//    print_r($tmp_name);       //  C:\xampp\tmp\phpA9D4.tmp
        $check = getimagesize($tmp_name);
        if ($check != false) {
            $uploadOK = 1;
        } else {
            echo 'File không phải định dạng ảnh';
            $uploadOK = -1;
            return -1;
        }

        if ($max > $max * 1024 * 1024) {
            $uploadOK = -1;
            echo ' file quá lớn. xin vui lòng kiểm tra lại';
            return -1;
        }

//    print_r($name);
//    echo '<br />';
//    print_r($type);
//    echo '<br />';
//    print_r($imageFileType);

        if (!in_array($imageFileType, $type)) {
            echo 'File không phải định dạng ảnh';
            $uploadOK = -1;
            return -1;
        }

        if (file_exists($targetfile)) {
            echo ' file đã tồn tại. xin vui lòng chọn file khác';
            $uploadOK = -1;
            return -1;

            /**
             * Xử lý TH 2 file giống nhau :
             * File nào upload sau thì mặc định sẽ được cộng thêm ở đầu 1 đoạn ký tự
             * Đoạn ký tự này sẽ 1 hàm ramdom trả về 10  ký tự( số ký tự tùy theo cấu hình)
             * Do đó tránh tình trạng upload trùng file ( ko bị replace )
             */
        }

        if ($uploadOK != 1) {
            echo 'upload file không thành công';
            return -1;
        } else {
            if (move_uploaded_file($tmp_name, $targetfile)) {
                return 1;
            } else {
                echo 'upload không thành công';
                return -1;
            }
        }
    }


?>