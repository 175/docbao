

<link rel="stylesheet" type="text/css" href="css/css.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
<script src="../bootstrap-3.3.7-dist/jquery.min.js"></script>
<script src="../bootstrap-3.3.7-dist/jquery-2.1.0.min.js"></script>
<script src="../bootstrap-3.3.7-dist/jquery-3.2.1.min.js"></script>
<script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script src="js/ajax.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<div>
<?php
require("connection.php");
$IdUser = '';
if ($_SESSION["IdUser"])
    $IdUser = $_SESSION["IdUser"];



    if (isset($_POST["btn_submit_tintuc"])) {
        // lấy thông tin :
        $TieuDe = $_POST["TieuDe"];
        $TomTat = $_POST["TomTat"];
        $NoiDung = $_POST["NoiDung"];
        $nameImages = $_FILES["UrlImages"]["name"];
        $size = $_FILES["UrlImages"]["size"];
        $tmp_name = $_FILES["UrlImages"]["tmp_name"];
        $folder = "../images/tintuc/";
        $IdLoaiTin = $_POST["IdLoaiTin"];
        $IdBienTap = $_POST["IdBienTap"];
        $idTheLoai = $_POST["idTheLoai"];
        $TinNoiBat = $_POST["TinNoiBat"];
        $TimeDangBai =  date("y-m-d h:m:s" );
        $TinTieuDiem = $_POST["TinTieuDiem"];
        $NguonTin = $_POST["NguonTin"];
        $Images = $folder . $nameImages;

        if (!($size < 3 * 1024 * 1024)) {
            echo 'file < 3MB. Vui lòng chọn file khác';
        } else {
            $kt = move_uploaded_file($tmp_name, $Images);
            if ($kt) {
                if ($TieuDe == "" || $TomTat == "" || $NoiDung == "") {
                    echo 'Vui lòng nhập đầy đủ thong tin yêu cầu';
                } else {
                    $sql = "SELECT * FROM tintuc WHERE TieuDe = '$TieuDe' AND TomTat = '$TomTat' AND NoiDung = '$NoiDung'";

                    $kt = mysqli_query($connect, $sql);
                    if ( mysqli_num_rows($kt) > 0) {
                        echo 'Tài khoản đã tồn tại';
                    }  else {
                        $sql = "INSERT INTO tintuc (
                        TieuDe, 
                        TomTat, 
                        NoiDung, 
                        UrlImages, 
                        IdLoaiTin, 
                        IdUser, 
                        IdBienTap, 
                        idTheLoai, 
                        TinNoiBat,
                         TimeDangBai, 
                         TinTieuDiem , 
                          NguonTin
                          ) 
                        VALUES (
                        '$TieuDe', '$TomTat', '$NoiDung', '$nameImages', 
                        '$IdLoaiTin', '$IdUser', 
                        '$IdBienTap', '$idTheLoai', '$TinNoiBat', '$TimeDangBai', '$TinTieuDiem' , '$NguonTin'               
                        )";


                        if (mysqli_query($connect, $sql)) {
                            sleep(1);
                            header('Location: index.php?p=tintuc');
                        } else {
                            echo 'thất bại';
                        }
                    }
                }
            } else {
                echo 'upload không thành công. Xin vui lòng thử lại';
            }
        }



    }
?>

    <h4 style="color: white">Thêm thông tin tin tức</h4>
    <form method="post" name="form_tintuc" enctype="multipart/form-data">
        <table class="table" style="color: #000">
            <caption style="font-size: 20px; color: white ">Mời nhập đầy đủ các trường dữ liệu </caption>

            <input type="hidden" name="IdTinTuc">
            <tr>
                <td>Tiêu đề</td>
                <td><input type="text" name="TieuDe"></td>
            </tr>

            <tr>
                <td>Tóm tắt</td>
                <td><input type="text" name="TomTat"></td>
            </tr>

            <tr>
                <td>Nội dung</td>
                <td>
<!--                    <input type="text" name="NoiDung">-->
                    <textarea name="NoiDung" id="editor" cols="80" rows="10">
                        nội dung ...
                    </textarea>
                    <script>
                            CKEDITOR.replace('editor');
                    </script>
                </td>
            </tr>
            <tr>
                <td>Hình Ảnh</td>
                <td><input type="file" name="UrlImages"></td>
            </tr>
            <tr>
                <td>Loại tin</td>
                <td>
                    <select name="IdLoaiTin" >
                        <?php
                            $sql = "select * from loaitin";
                            $result = mysqli_query($connect, $sql);
                            while ($data = mysqli_fetch_array($result)) {
                        ?>
                        <option value="<?php echo $data["IdLoaiTin"];?>"><?php echo $data["TenLT"] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Biên tập</td>
                <td>
                    <select name="IdBienTap" id="">
                    <?php
                    $sql = "select * from banbientap";
                    $result = mysqli_query($connect, $sql);
                    while ($data = mysqli_fetch_array($result)) {
                    ?>
                        <option value="<?php echo $data["IdBienTap"];?>"><?php echo $data["HoTen"];?></option>
                    <?php
                    }
                    ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Thể loại</td>
                <td>
                    <select name="idTheLoai" id="">
                        <?php
                        $sql = "select * from theloai";
                        $result = mysqli_query($connect, $sql);
                        while ($data = mysqli_fetch_array($result)) {
                            ?>
                            <option value="<?php echo $data["idTheLoai"];?>"><?php echo $data["TenTheLoai"];?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Tin nóng</td>
                <td>
                    <select name="TinNoiBat" id="">
                        <option value="1">Có</option>
                        <option value="0">Không </option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tin Tiêu Điểm trong tuần</td>
                <td>
                    <select name="TinTieuDiem" id="">
                        <option value="1">Có</option>
                        <option value="0">Không </option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Nguồn tin</td>
                <td>
                    <input type="text" name="NguonTin">
                </td>
            </tr>
            <tr>
                <td colspan="1" align="center">
                    <input type="submit" name="btn_submit_tintuc" value="   Thêm    ">
                </td>
            </tr>

        </table>
    </form>
</div>