<?php
$theloai = getDanhSachTheLoai(10);
$row_the_loai = mysqli_fetch_assoc($theloai);
$idTL = $row_the_loai['idTheLoai'];
?>
<div class="news_box_type3">
    <h4 class="title">
        <a href="index.php?p=tintrongtheloai&idTL=<?php echo $row_the_loai['idTheLoai']?>">
            <?php echo $row_the_loai['TenTheLoai'] ?>
        </a>
    </h4>
    <div class="main_ct">

        <?php
            $tintuc_gioitre = getGioiTre_GiaDinh($idTL);
            $tintuc_gioitre_show = mysqli_fetch_assoc($tintuc_gioitre);
            ?>
        <div class="news_big">
            <a href="index.php?p=chitiettin&idTL=<?php echo $tintuc_gioitre_show['idTheLoai']?>&idTinTuc=<?php echo $tintuc_gioitre_show['IdTinTuc'] ?>">
                <img width="340" height="255" src="images/tintuc/<?php echo $tintuc_gioitre_show['UrlImages'] ?>"
                     alt="<?php
                     echo $tintuc_gioitre_show['TieuDe']
                     ?>">
                <span><b><?php
                        echo $tintuc_gioitre_show['TieuDe']
                        ?>
                    </b>
                </span>
            </a>
        </div>

        <div class="news_r">
            <?php
            $tintuc_gioitre = getGioiTre_GiaDinh_mottin($idTL);
            $tintuc_gioitre_show = mysqli_fetch_assoc($tintuc_gioitre);
            ?>
            <div class="top">
                <a class="photo"
                   href="index.php?p=chitiettin&idTL=<?php echo $tintuc_gioitre_show['idTheLoai']?>&idTinTuc=<?php echo $tintuc_gioitre_show['IdTinTuc'] ?>">
                    <img width="120" height="90"
                         src="images/tintuc/<?php
                         echo $tintuc_gioitre_show['UrlImages']
                         ?>"
                         alt="<?php
                         echo $tintuc_gioitre_show['TieuDe']
                         ?>"></a>
                <h6>
                    <a href="index.php?p=chitiettin&idTL=<?php echo $tintuc_gioitre_show['idTheLoai']?>&idTinTuc=<?php echo $tintuc_gioitre_show['IdTinTuc'] ?>"><?php
                        echo $tintuc_gioitre_show['TieuDe']
                        ?></a></h6>
                <div class="clrb">
                </div>
            </div>


        <?php
        $tintuc_gioitre = getGioiTre_GiaDinh_BenPhai($idTL);
        while ($tintuc_gioitre_show = mysqli_fetch_assoc($tintuc_gioitre)){
        ?>
            <ul>
                <li>
                    <a href="index.php?p=chitiettin&idTL=<?php echo $tintuc_gioitre_show['idTheLoai']?>&idTinTuc=<?php echo $tintuc_gioitre_show['IdTinTuc'] ?>">
                        <?php
                        echo $tintuc_gioitre_show['TieuDe']
                        ?>
                    </a>
                </li>
            </ul>
        <?php
            }
        ?>
        </div>
        <div class="clrb">
        </div>

    </div>
</div>