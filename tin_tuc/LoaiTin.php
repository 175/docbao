
<?php
$idLoaiTin = $_GET["idLoaiTin"];
settype($idLoaiTin, "int");
?>
<?php
$tin_lt = getTenLoaiTin($idLoaiTin);
$row_tin = mysqli_fetch_assoc($tin_lt);
?>
<div class="category_page">
    <h5 class="forum_name">
        <span><?php echo $row_tin["TenLT"] ?></span></h5>

    <div class="category_news_top">
        <ul>

            <?php
            $tin_lt = TinTheoLoaiTin_HaiTin($idLoaiTin);

            while ($row_tin = mysqli_fetch_assoc($tin_lt)) {
                ?>
                <li>
                    <a class="photo" href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                        <img width="340" height="255"
                             src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>"
                             alt="<?php echo $row_tin["TieuDe"] ?>"></a>
                    <h4>
                        <a href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                            <?php echo $row_tin["TieuDe"] ?></a></h4>
                    <p><?php echo $row_tin["TomTat"] ?></p>
                </li>
                <?php
            }
            ?>

        </ul>
        <div class="clrb">
        </div>
    </div>

    <div class="category_news_list">
        <ul>


            <?php
            $sotin1trang = 5;

            if (isset($_GET["trang"])) {
                $trang = $_GET["trang"];
                settype($trang, "int");
            } else {
                $trang = 1;
            }
            $from = ($trang - 1) * $sotin1trang + 2;
            $tin = TinTheoLoaiTin_PhanTrang($idLoaiTin, $from, $sotin1trang);
            while ($row_tin = mysqli_fetch_assoc($tin)) {
                ?>
                <li><a class="photo" href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                        <img width="206"
                             src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>"
                             alt="<?php echo $row_tin['TieuDe'] ?>"></a>
                    <div class="text">
                        <h5>
                            <a href="index.php?p=chitiettin&idTL=<?php echo $row_tin['idTheLoai']?>&idTinTuc=<?php echo $row_tin['IdTinTuc'] ?>">
                                <?php echo $row_tin['TieuDe'] ?></a></h5>
                        <span class="time"><?php echo $row_tin['TimeDangBai'] ?></span>
                        <p><?php echo $row_tin['TomTat'] ?> </p>
                    </div>
                    <div class="clrb">
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>

    <?php
    $i = 1;
    $i++;

    ?>
    <div style="padding: 0 0" class="button_bot">
            <span><a class="btn_next"
                     href="index.php?p=tintrongtheloai&idTL=<?php echo $idLoaiTin ?>&trang=<?php echo $i ?>">
                    Xem thêm »</a></span>
    </div>
</div>