<script type="text/javascript" src="js/ajax.js"></script>
<link href="css/css.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
<style>
    .read-more-state {
        display: none;
    }

    .read-more-target {
        opacity: 0;
        max-height: 0;
        font-size: 0;
        transition: .25s ease;
    }

    .read-more-state:checked ~ .read-more-wrap .read-more-target {
        opacity: 1;
        font-size: inherit;
        max-height: 999em;
    }

    .read-more-state ~ .read-more-trigger:before {
        content: 'Xem Thêm';
    }

    .read-more-state:checked ~ .read-more-trigger:before {
        content: 'Ẩn';
    }

    .read-more-trigger {
        cursor: pointer;
        display: inline-block;
        padding: 0 .5em;
        color: #666;
        font-size: .9em;
        line-height: 2;
        border: 1px solid #ddd;
        border-radius: .25em;
    }

    /* Other style */
    body {
        /*padding: 2%;*/
    }

    p {
        /*padding: 2%;
        background: #fff9c6;
        color: #c7b27e;
        border: 1px solid #fce29f;
        border-radius: .25em;*/
    }

    a {
        color: #0254EB
    }

    a:visited {
        color: #0254EB
    }

    a.morelink {
        text-decoration: none;
        outline: none;
    }

    .morecontent span {
        display: none;
    }

    .comment {
        width: 400px;
        background-color: #f0f0f0;
        margin: 10px;
        padding: 10px;
    }

</style>
<?php
require("connection.php");
if (isset($_SESSION["QuyenUser"]))
    $quyen = $_SESSION["QuyenUser"];
$IdUser = $_SESSION["IdUser"];
?>
<div style="margin-left: 0px;">
    <div style="overflow:scroll">

        <h3>Tin nóng trong ngày</h3>
        <h3><a href="index.php?p=themtintuc" style="color: white">Thêm mới Tin tức nóng</a></h3>
        <caption style="color: #8e0e12"></caption>
        <form action="" method="post">
            <table style="color: black">
                <tr>
                    <td style="color: #8e0e12; font-weight: bold; font-size: 18px;"> Lọc theo thể loại:</td>
                    <td>
                        <select name="idTheLoai" id="">
                            <option value="allTheLoai">
                                Tất cả
                            </option>
                            <?php
                            $sql = "select * from theloai";
                            $query = mysqli_query($connect, $sql);
                            while ($data = mysqli_fetch_assoc($query)) {
                                ?>
                                <option value="<?php echo $data["idTheLoai"]; ?> "> <?php echo $data["TenTheLoai"] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td><input type="submit" name="" value="Tìm kiếm Thể Loại"></td>

                    <td style="color: #761c19; font-weight: bold; font-size: 18px;margin-right:2px;"> Lọc theo thời
                        gian:
                    </td>
                    <td>
                        <select name="TimeDangBai" id="">
                            <option value="allThoiGian">
                                Tất cả
                            </option>
                            <?php
                            $i = 0;
                            $time = array();
                            $sql = "select * from tintuc";
                            $query = mysqli_query($connect, $sql);
                            while ($data = mysqli_fetch_assoc($query)) {
                                ?>
                                <option value="<?php

                                $data['TimeDangBai'] = date('d-m-y');
                                $time[$i] = $data['TimeDangBai'];
                                if ($time[$i + 1] != $time[$i])
                                    echo $data["idTheLoai"]; ?>
                             $i++; ">

                                    <?php echo $data["TimeDangBai"]; ?>

                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td><input type="submit" name="" value="Tìm kiếm Theo TG"></td

                </tr>

            </table>
        </form>

        <table class="table" style="color: #0f0f0f">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tiêu đề</th>
                <th>Tóm tắt</th>
                <th>Nội dung</th>
                <th>Hình Ảnh</th>
                <th>Mã loại tin</th>
                <th>Mã nhân viên</th>
                <th>Mã biên Tập</th>
                <th>Mã thể loại</th>
                <th>Tin nổi bật?</th>
                <th>Thời gian</th>
                <th>Số lượt xem</th>
                <th>Tin tiêu điểm</th>
                <th>Nguồn tin</th>
                <th>Thao tác</th>
            </tr>
            </thead>

            <tbody>
            <?php
            $display = 10;
            $start = 0;

            if (isset($_GET['page']) && (int)$_GET['page']) {
                $page = $_GET['page'];
            } else {

                $sql_count = "SELECT COUNT(IdTinTuc) FROM tintuc ";
                $res = mysqli_query($connect, $sql_count) or die('could not find the loai' . mysqli_error());
                $row = mysqli_fetch_array($res, MYSQLI_NUM);
                $record = $row[0];
//            exit( 'record: ' . $record);    // số bản ghi
                if ($record > $display) {
                    $page = ceil($record / $display);
                } else {
                    $page = 1;
                }

                $start = (isset($_GET['start']) && (int)$_GET['start']) ? $_GET['start'] : 0;
//            $sql_limit = "SELECT * FROM theloai tintuc LIMIT $start, $display";

            }


            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                // Thiết lập mảng lưu lỗi.. mặc định rỗng
//            exit('1111111111111');
                $error = array();
                if (empty($_POST['idTheLoai'])) {
                    $error['idTheLoai'] = 'Lựa chọn thể loại';
                } else {
                    $idTheLoai = $_POST['idTheLoai'];
                }
                // kiểm tra có lỗi hay ko
                if (empty($error)) {
                    if ($idTheLoai == "allTheLoai") {

                        $stt = 1;
                        $sql = "SELECT * FROM tintuc ORDER BY tintuc.IdTinTuc DESC LIMIT $start, $display";
//                    exit('all:  ' . $sql);
                        // Thực thi câu lệnh $sql với biến connect tù file connection
                        $query = mysqli_query($connect, $sql);
                    } else {

                        $stt = 1;
                        $sql = "SELECT * FROM tintuc WHERE tintuc.idTheLoai = '$idTheLoai' ORDER BY tintuc.IdTinTuc DESC LIMIT $start, $display";
                        // Thực thi câu lệnh $sql với biến connect tù file connection
//                    exit('1111  '  .$sql);
                        $query = mysqli_query($connect, $sql);
                    }
                }
            } else {

                $stt = 1;
                $sql = "SELECT * FROM tintuc ORDER BY tintuc.IdTinTuc DESC LIMIT $start, $display";
                // Thực thi câu lệnh $sql với biến connect tù file connection
                $query = mysqli_query($connect, $sql);
            }
            while ($data = mysqli_fetch_array($query)) {
                ?>
                <tr>
                    <th scope="row"><?php echo $stt++ ?></th>
                    <div>
                        <td> <?php echo $data["TieuDe"]; ?></td>
                    </div>

                    <div>
                        <td><?php echo $data["TomTat"]; ?></td>
                    </div>

                    <td class="comment">
                        <?php echo $data["NoiDung"]; ?>
                    </td>

                    <td><?php echo '<img src=' . '../images/tintuc/' . $data['UrlImages'] . ' style="width: 50px; height: 50px;"/> '; ?></td>

                    <td><?php
                        $IdLoaiTin = $data["IdLoaiTin"];
                        $sql1 = "select TenLT from loaitin WHERE IdLoaiTin = $IdLoaiTin";
                        $query1 = mysqli_query($connect, $sql1);
                        $TenLoaiTin = '';
                        $dataLoaiTin = mysqli_fetch_assoc($query1);
                        $TenLoaiTin = $dataLoaiTin["TenLT"];
                        if (!isset($TenLoaiTin)) {
                            echo 'null';
                        }
                        echo $TenLoaiTin; ?></td>

                    <td><?php
                        $IdUser = $data["IdUser"];
                        $sql2 = "select HoTen from user WHERE IdUser = $IdUser";
                        $query2 = mysqli_query($connect, $sql2);
                        $HoTen = '';
                        $dataUser = mysqli_fetch_assoc($query2);
                        $HoTen = $dataUser["HoTen"];
                        if (!isset($HoTen)) {
                            echo 'null';
                        }
                        echo $HoTen; ?></td>

                    <td><?php $IdBienTap = $data["IdBienTap"];
                        $sql3 = "select HoTen from banbientap WHERE IdBienTap = $IdBienTap";
                        $query3 = mysqli_query($connect, $sql3);
                        $HoTen = '';
                        $dataBientap = mysqli_fetch_assoc($query3);
                        $HoTen = $dataBientap["HoTen"];
                        if (!isset($HoTen)) {
                            echo 'null';
                        }
                        echo $HoTen; ?></td>

                    <td><?php
                        $idTheLoai = $data["idTheLoai"];
                        $sql4 = "select TenTheLoai from theloai WHERE idTheLoai = $idTheLoai";
                        $query4 = mysqli_query($connect, $sql4);
                        $TenTheLoai = '';
                        $datatheloai = mysqli_fetch_assoc($query4);
                        $TenTheLoai = $datatheloai["TenTheLoai"];
                        if (!isset($TenTheLoai)) {
                            echo 'null';
                        }
                        echo $TenTheLoai; ?></td>

                    <td><?php
                        // echo $data["TinNoiBat"];
                        if ($data["TinNoiBat"] == 0) {
                            echo 'Không';
                        } else if ($data["TinNoiBat"] == 1) {
                            echo 'Có';
                        }
                        ?></td>

                    <td><?php echo $data["TimeDangBai"] ?></td>

                    <td>
                        <?php echo $data["ViewTinTuc"] ?>
                    </td>

                    <td>
                        <?php
                        if ($data["TinTieuDiem"] == 1) echo 'Có';
                        else if ($data["TinTieuDiem"] == 0) echo 'Không'; ?>
                    </td>

                    <td>
                        <?php echo $data["NguonTin"]; ?>
                    </td>

                    <td>
                        <div id="add_insert_delete">
                        <span id="">
                             <a href="index.php?p=edittintuc&id=<?php echo $data["IdTinTuc"]; ?> ">
                                 <?php
                                 $idNV = $data["IdUser"];
                                 $sql_ed = "Select QuyenUser from user WHERE IdUser = '$idNV'";
                                 $query_ed = mysqli_query($connect, $sql_ed);
                                 while ($data_ed = mysqli_fetch_assoc($query_ed)) {
                                     if ($data_ed["QuyenUser"] == $quyen || $data_ed["QuyenUser"] == 3) {
                                         echo 'Chỉnh sửa';
                                     } else {
                                         echo '';
                                     }
                                 }
                                 ?>
                             </a> </span> -
                            <span id="">
                             <a href="xoa-tin-tuc.php?id=<?php echo $data["IdTinTuc"]; ?>">
                                 <?php
                                 $idNV = $data["IdUser"];
                                 $sql_ed = "Select QuyenUser from user WHERE IdUser = '$idNV'";
                                 $query_ed = mysqli_query($connect, $sql_ed);
                                 while ($data_ed = mysqli_fetch_assoc($query_ed)) {
                                     if ($data_ed["QuyenUser"] == $quyen || $data_ed["QuyenUser"] == 3) {
                                         echo 'Xóa';
                                     } else {
                                         echo '';
                                     }
                                 }
                                 ?>
                                 </a></span>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>

        <ul class="phantrang" style="display: block; color: red; background-color: white">
            <li>
                <?php

                if ($page > 1) {
                    $next = $start + $display;
                    $prev = $start - $display;
                    $current = ($start / $display) + 1;
//                echo   'page'. $page . '  start ' . $start . '   prev' .$prev . '   ' ;

                    if ($current != 1) {
                        echo "<a class='pagination' href='quan-ly-tin-tuc.php?start=$prev'>Previous - </a>";
                    }
                    // Hiển thị số link
                    for ($i = 1; $i < $page; $i++) {
                        echo "<a class='pagination' href='quan-ly-tin-tuc.php?start=" . ($display * ($i - 1)) . "'>  Trang $i - </a>";
                    }

                    // Hiển thị trang nexxt
                    if ($current != $page) {
                        echo "<a class='pagination' href='quan-ly-tin-tuc.php?start=$next'>Next</a>";
                    }
                }
                ?>
            </li>
        </ul>

        <script type="text/javascript" src="js//jquery-latest.js"></script>
        <script type="text/javascript" src="js/jquery.shorten.1.0.js"></script>

        <script type="text/javascript">
            $(".comment").shorten({
                "showChars": 20,
                "moreText": "Xem thêm",
                "lessText": "Ẩn",
            });
        </script>
    </div>
</div>
