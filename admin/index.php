<?php
ob_start();
session_start();
if (!isset($_SESSION["username"])) {
    header("Location: login.php");
} else {
    $name = $_SESSION["username"];
    $quyen = $_SESSION["QuyenUser"];
    $IdUser = $_SESSION["IdUser"];
}

if (isset($_GET["p"])) {
    $p = $_GET["p"];
} else {
    $p = "";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" type="text/css" href="css/css.css">
    <link rel="stylesheet" type="text/css" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../bootstrap-3.3.7-dist/jquery.min.js"></script>
    <script src="../bootstrap-3.3.7-dist/jquery-2.1.0.min.js"></script>
    <script src="../bootstrap-3.3.7-dist/jquery-3.2.1.min.js"></script>
    <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="js/ajax.js"></script>

    <style>
        body {
            font-family: Arial, Tahoma;
            font-size: 16px;
        }
        #main {
            width: 968px;
            padding: 0px;
            background-color: #D3D3D3;
            margin-top: 10px;
            margin-left: 10%;
            margin-right: 10%;


        }

        .header {
            height: 40px;
            border: 1px solid #CDCDCD;
            margin-bottom: 5px;
            margin-left: 1%;
        }

        .headerlink {
            height: 30px;
            line-height: 30px;
            padding-right: 10px;
            border: 1px solid #CDCDCD;
            background-color: #F5F5F5;
            margin-bottom: 5px;
            clear: both;
        }

        .container {
            width: 1000px;
            min-height: 400px;
            border: 1px solid #CDCDCD;
            margin: 0px 5px 5px 5px;
            
        }

        .footer {

            height: 60px;
            clear: both;
            border: 1px solid #CDCDCD;
            background-color: #F8F8FF;
            margin-bottom: 5px;
            clear: both;
        }
    </style>

</head>
<body background="images/background1.jpg">

<?php
    require ("connection.php");
?>
<div id="main">

    <div class="header">
        <a href="index.php" style="color: #004074; font-weight: bold;font-size: 18px ">TRANG QUẢN TRỊ QUẢN LÝ TIN TỨC</a>

<!--        <input style="float: right; margin-right: 10px; margin-top: 2px;" type="button" name="logout" value="Đăng Xuất" >-->
        <a style="float: right; margin-right: 10px; margin-top: 2px; color: #000000" href="logout.php">Đăng Xuất</a>

    </div>

    <div class="headerlink">
        <ul class="menu_row">
            <a href="index.php">Trang Chủ</a> ||
            <a href="index.php?p=banbientap">Ban Biên Tập</a> ||
            <a href="index.php?p=loaitin">Loại Tin</a> ||
            <a href="index.php?p=quangcao">Quảng Cáo</a> ||
            <a href="index.php?p=thanhvien">Thành Viên</a> ||
            <a href="index.php?p=theloai">Thể Loại</a> ||
            <a href="index.php?p=tintuc">Tin Tức</a> ||
            <a href="index.php?p=tranglienket">Trang Liên Kết</a> ||
            <a href="#">Phân Quyền</a>
        </ul>
    </div>

    <div class="container" style="width: 968px" >
        <div class="table" style="margin-top: 0px" >
            <div id="contain" style="background-color: #0a86ab; color: #FFFFFF;" >
                <div id="noidung">
                    <?php

                    switch ($quyen) {
                        case 1:
                            switch ($p){
                                case 'theloai':
                                    include "quan-ly-the-loai.php";
                                    break;
                                case 'themtheloai':
                                    include "them-the-loai.php";
                                    break;
                                case 'edittheloai':
                                    include "chinh-sua-the-loai.php";
                                    break;


                                case 'banbientap':
                                    include "quan-ly-ban-bien-tap.php";
                                    break;
                                case 'thembanbientap':
                                    include "them-ban-bien-tap.php";
                                    break;
                                case 'editbanbientap':
                                    include "chinh-sua-ban-bien-tap.php";
                                    break;

                                case 'tintuc':
                                    include "quan-ly-tin-tuc.php";
                                    break;
                                case 'themtintuc':
                                    include "them-tin-tuc.php";
                                    break;
                                case 'edittintuc':
                                    include "chinh-sua-tin-tuc.php";
                                    break;


                                default:
                                    include "admin.php";    // thể loại
                                    break;
                            }
                            break;

                        case 2:
                            switch ($p){

                                case 'loaitin':
                                    include "quan-ly-loai-tin.php";
                                    break;
                                case 'themloaitin':
                                    include "them-loai-tin.php";
                                    break;
                                case 'editloaitin':
                                    include "chinh-sua-loai-tin.php";
                                    break;


                                case 'tranglienket':
                                    include "quan-ly-trang-lien-ket.php";
                                    break;
                                case 'themtranglk':
                                    include "them-lien-ket.php";
                                    break;
                                case 'editlienket':
                                    include "chinh-sua-lien-ket.php";
                                    break;


                                case 'quangcao':
                                    include "quan-ly-quang-cao.php";
                                    break;

                                case 'themquangcao':
                                    include "them-quang-cao.php";
                                    break;
                                case 'chinhsuaquangcao':
                                    include "chinh-sua-quang-cao.php";
                                    break;





                                case 'tintuc':
                                    include "quan-ly-tin-tuc.php";
                                    break;
                                case 'themtintuc':
                                    include "them-tin-tuc.php";
                                    break;
                                case 'edittintuc':
                                    include "chinh-sua-tin-tuc.php";
                                    break;

                                default:
                                    include "quan-ly-loai-tin.php";
                                    break;
                            }
                            break;

                        case 3:
                            switch ($p){
                                case 'thanhvien':
                                    include "quan-ly-thanh-vien.php";
                                    break;
                                case 'banbientap':
                                    include "quan-ly-ban-bien-tap.php";
                                    break;

                                case 'loaitin':
                                    include "quan-ly-loai-tin.php";
                                    break;

                                case 'quangcao':
                                    include "quan-ly-quang-cao.php";
                                    break;
                                case 'themquangcao':
                                    include "them-quang-cao.php";
                                    break;
                                case 'chinhsuaquangcao':
                                    include "chinh-sua-quang-cao.php";
                                    break;

                                case 'theloai':
                                    include "quan-ly-the-loai.php";
                                    break;

                                case 'tintuc':
                                    include "quan-ly-tin-tuc.php";
                                    break;

                                case 'tranglienket':
                                    include "quan-ly-trang-lien-ket.php";
                                    break;

                                case 'thembanbientap':
                                    include "them-ban-bien-tap.php";
                                    break;

                                case 'editbanbientap':
                                    include "chinh-sua-ban-bien-tap.php";
                                    break;

                                case 'themloaitin':
                                    include "them-loai-tin.php";
                                    break;

                                case 'editloaitin':
                                    include "chinh-sua-loai-tin.php";
                                    break;

                                case 'editthanhvien':
                                    include "chinh-sua-thanh-vien.php";
                                    break;

                                case 'themtintuc':
                                    include "them-tin-tuc.php";
                                    break;

                                case 'edittintuc':
                                    include "chinh-sua-tin-tuc.php";
                                    break;

                                case 'themtranglk':
                                    include "them-lien-ket.php";
                                    break;

                                case 'editlienket':
                                    include "chinh-sua-lien-ket.php";
                                    break;

                                case 'themtheloai':
                                    include "them-the-loai.php";
                                    break;
                                case 'edittheloai':
                                    include "chinh-sua-the-loai.php";
                                    break;

                                default:
                                    include "admin.php";
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                    ?>
                </div>

            </div>
        </div>
    </div>

    <div class="footer" style="margin-top: 100px">
            <p>
                Trang tin tức điện tử của : docbao.vn &nbsp; 175 Tây Sơn Đông Đa Hà Nội
            </p>
            <p style="margin-top: 10px;">
                Liên hệ <a href="#">Quảng cáo</a> | <a href="#">Thông tin
                </a>
            </p>
    </div>

</div>

</body>
</html>