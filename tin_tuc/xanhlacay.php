    <?php
    $sotin1trang = 5;

    if( isset($_GET["trang"]) ){
        $trang = $_GET["trang"];
        settype($trang, "int");
    }else{
        $trang = 1;
    }
    $from = ($trang - 1) * $sotin1trang + 2;
    $tin = TinTheoTheLoaiTin_PhanTrang($idLT, $from, $sotin1trang);
    while ($row_tin = mysqli_fetch_assoc($tin)) {
        ?>
        <li>
            <a class="photo"
               href="/tin-tuc/20-06-2017/duong-day-nong-cua-thanh-tra-thi-thpt-quoc-gia-2017-hoat-dong-tu-hom-nay/29/461628">
                <img width="206"
                     src="images/tintuc/<?php echo $row_tin['UrlImages'] ?>"
                     alt="<?php echo $row_tin['TieuDe'] ?>"></a>
            <div class="text">
                <h5>
                    <a href="/tin-tuc/20-06-2017/duong-day-nong-cua-thanh-tra-thi-thpt-quoc-gia-2017-hoat-dong-tu-hom-nay/29/461628">
                        <?php echo $row_tin['TieuDe'] ?>
                    </a></h5>
                <span class="time"><?php echo $row_tin['TimeDangBai'] ?></span>
                <p><?php echo $row_tin['TomTat'] ?></p>
            </div>
            <div class="clrb">
            </div>
        </li>
        <?php
    }
    ?>