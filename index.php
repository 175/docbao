<?php
include "model/function_data.php";
include "model/m_tintuc.php";

if (isset($_GET["p"]))
    $p = $_GET["p"];
else
    $p = "";
?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Đọc Báo - Website Tin tức Điện tử - Báo điện tử - Tin nhanh - Tin mới hàng ngày - Tin tức 24h |
        DocBao.Vn</title>

    <!-- CSS -->
    <!--    <link type="text/css" rel="stylesheet" href="css/reset.css"/>-->
    <link type="text/css" rel="stylesheet" href="css/docbao.css"/>
    <link type="text/css" rel="stylesheet" href="css/styte.css"/>
    <link type="text/css" rel="stylesheet" href="css/trangcon.css"/>
    <link type="text/css" rel="stylesheet" href="css/panda.css"/>
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
    <!-- JAVASCRIPT -->
    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/panda.js"></script>

    <meta property="fb:app_id" content="382248452171272"/>
    <meta property="fb:admins" content="100008298311300"/>

</head>

<body>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=382248452171272";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '382248452171272',
            xfbml: true,
            version: 'v2.9'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<form id="form1">
    <div>
        <img id="image" src="images/cute_panda_drawing_by_arycarys-d81yy6k.png"/>
    </div>
    <?php require "tin_tuc/tincon/nav_header_menu.php" ?>

    <div class="wrapper">

        <div class="header">
            <div class="logozone">
                <a alt='Doc bao' href="/" class="logo">
                    <img alt="Logo docbao.vn" width="250px" src="images/home_logo_header1.jpg"/>
                </a>
                <div class="advbox">
                    <div class="ad" style="float: left">

                        <a href="https://www.uber.com/signup/drive/hanoi/?utm_source=display&utm_campaign=display-dbm_190_429_vn-hanoi_d_all_acq_cpm_vi-vn_pros&utm_medium=2017-170226_Q1-APACx-Driver_Set-3_728x90&dclid=CNjTl-Tq4NQCFQMTvQodaj8Csw">
                            <img src="images/uber.png">
                        </a>
                        <!--                        --><?php
                        //                            $quangcao = quangcao(1);
                        //                            $row_quangcao = mysqli_fetch_assoc($quangcao);
                        //                        ?>
                        <!--                        <a target="black" href="-->
                        <?php //echo $row_quangcao['UrlQuangCao'] ?><!--">-->
                        <!--                        <img src="images/-->
                        <?php //echo $row_quangcao['UrlImages'] ?><!--"-->
                        <!---->
                        <!--                             href="--><?php //echo $row_quangcao['UrlQuangCao'] ?><!--"/>-->
                        <!--                        </a>-->

                    </div>
                </div>
                <div class="clrb">
                </div>
            </div>

            <ul class="menubar">
                <li><a href="index.php" class="home on"><span></span></a></li>

                <?php include "tin_tuc/menu_top.php" ?>


                <div class="clrb">
                </div>
            </ul>
        </div>

        <div id="abdMastheadMb" style="margin:0 auto; width:100%;">
        </div>

        <div class="advfullwidth" width="300px">

            <a href="https://friso.asia/v2/?utm_expid=.by2NlExASqijc7WCSlITKw.1&utm_referrer=">
                <img src="images/quangcao_duoimenu.png">
            </a>
            <!--            --><?php
            //            $quangcao = quangcao(2);
            //            $row_quangcao = mysqli_fetch_assoc($quangcao);
            //            ?>
            <!--            <a target="black" href="--><?php //echo $row_quangcao['UrlQuangCao'] ?><!--">-->
            <!--                <img src="images/--><?php //echo $row_quangcao['UrlImages'] ?><!--"-->
            <!---->
            <!--                     href="--><?php //echo $row_quangcao['UrlQuangCao'] ?><!--"/>-->
            <!--            </a>-->
        </div>

        <div class="layout2col">

            <div class="col_right">
                <?php include "tin_tuc/col_right.php" ?>
            </div>

            <div class="col_left">
                <?php
                switch ($p) {
                    case "tintrongtheloai":
                        include "tin_tuc/tin_theloai.php";
                        break;
                    case "chitiettin":
                        include "tin_tuc/chitiettin.php";
                        break;
                    case "lienhe":
                        include "tin_tuc/lienhe.php";
                        break;
                    case "timkiem":
                        include "tin_tuc/timkiem.php";
                        break;
                    default:
                        include "tin_tuc/trangchu.php";
                }
                ?>
            </div>

        </div>

        <div align="center" style="clear: both;">

        </div>
    </div>


    <div class="footer">
        <div class="footer_nav">
            <div class="footer_nav_ct">
                <a href="#" class="home"><span></span></a>
                <?php
                $menu = getMenu();
                // mysqli_fetch_assoc: tìm và trả về một dòng giá trị của một mảng kết hợp
                while ($menu_item = mysqli_fetch_assoc($menu)) {
                    ?>
                    <a href="index.php?p=tintrongtheloai&idTL=<?php echo $menu_item['idTheLoai'] ?>">
                        <?php echo $menu_item["TenTheLoai"] ?></a>

                    <?php
                }
                ?>
            </div>
        </div>
        <div class="footer_ct">
            <a href="/" class="logo">
                <img alt="Logo docbao.vn" width="250px" src="images/home_logo_header1.jpg"/>
            </a>
            <div class="info">
                <p>
                    Trang tin tức điện tử của : docbao.vn
                </p>
                <p>
                    Công ty Cổ phần Abcxyz
                </p>
                <p>
                    Giấy phép...
                </p>
                <p>Vận hành bởi tổ chức abc</p>
                <p>Tòa nhà C1 175 Tây Sơn Đông Đa Hà Nội</p>
                <p>Chịu trách nhiệm nội dung: Nguyễn Văn A</p>
                <p style="margin-top: 10px;">
                    Liên hệ <a href="#">Quảng cáo</a> | <a
                            href="index.php?p=lienhe&idbientap =<?php echo $row_banbientap['IdBienTap'] ?> ">Thông tin
                        ban biên tập
                    </a>
                </p>
            </div>

        </div>
    </div>
    <div class="footer"><a class="btn-top" href="javascript:void(0);" title="Top" style="display: inline;"></a></div>
</form>

</body>
</html>
