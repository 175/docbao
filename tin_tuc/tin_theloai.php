
<?php
    if( isset($_GET["p_tl_lt"]))
        $p_tl_lt = $_GET["p_tl_lt"];
    else
        $p_tl_lt = "";
?>
<?php
$idTL = $_GET["idTL"];
settype($idTL, "int");
?>

<div class="col_left">

    <?php
        switch ($p_tl_lt){

            case "LoaiTin":
                include "LoaiTin.php";
                break;
            default:
                include "theloai.php";
        }

    ?>

    <br class="clear">

    <div class="detail_page">
        <div class="general_news">
            <div style="margin-right: 30px;" class="news_box">
                <h5>Tin tiêu điểm</h5>
                <ul>

                    <?php
                            $tintieudien = getTinTieuDiem($idTL);
                            while ($row_tieudien = mysqli_fetch_array($tintieudien)) {
                                ?>
                                <li><a href="index.php?p=chitiettin&idTL=<?php echo $row_tieudien['idTheLoai']?>&idTinTuc=<?php echo $row_tieudien['IdTinTuc'] ?>">
                                        <?php echo $row_tieudien['TieuDe'];
                                        ?>
                                        <span><i>(<?php echo $row_tieudien['TimeDangBai'];
                                                ?>)</i></span></a></li>
                                <?php
                            }
                    ?>
                </ul>
            </div>

            <div class="news_box">

                <h5>Tin khác</h5>
                <ul>
                    <?php
                    $tintieudien = getKhac($idTL);
                    while ($row_tieudien = mysqli_fetch_array($tintieudien)) {
                        ?>


                        <li><a href="index.php?p=chitiettin&idTL=<?php echo $row_tieudien['idTheLoai']?>&idTinTuc=<?php echo $row_tieudien['IdTinTuc'] ?>">
                               <?php  echo  $row_tieudien['TieuDe']?>
                                <span><i>(<?php  echo  $row_tieudien['TimeDangBai']?>)</i></span></a></li>
                        <?php
                    }
                    ?>

                </ul>
            </div>
            <div class="clrb">
            </div>
        </div>
        <script type="text/javascript">
            function goSearch() {
                window.location = '/doc-bao/xem-theo-ngay/' + $('#Day :selected').val() + '/' + $('#Mon :selected').val() + '/' + $('#Year :selected').val() + '/' + 29;
            }
        </script>
    </div>
</div>

